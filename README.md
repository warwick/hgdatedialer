**About This Library**

The purpose of this library is to replace the date picker dialog with something faster and superior; achieving maximum functionality with minimal input.

This deployment actually consists of 2 libraries HGDateDialer and HGTimeDialer

You can find the Youtube video for the demo app at: **https://youtu.be/mC4xnGt_1Dg**

**HGDateDialer User Instructions:**
This library has 3 out-of-the-box behaviours:

**Behaviour 1:**
The Dial is divided into 3 sections: Year dial in the centre, month dial in the middle and date dial towards the outside. The library detects what part of the dial the user is touching. Rotating the dial clockwise increases the date value and counter-clockwise decreases the date. When dialling the inner dial the year will change, touching the month part will change the month value and rotating the outside date part will change the date value. When rotating the year part of the dial, the library will detect how close the user is to the centre of the centre dial. The closer to the centre the faster the year value increments/decrements. This behaviour allows for date selection using only one gesture.

**Behaviour 2:**
This will be the same for behaviour 1 except that it uses 3 gestures. The first for the year, the second for the month and third for the date. When dialling the year (first gesture), dialling closer to the centre will change the date faster but unlike behaviour 1 the user can use the entire diameter of the dial.

**Behaviour 3:**
This behaviour will be exactly the same as behaviour 2 only the user will only see only the dial (full size) that they are dialling.

The demo app uses the old date picker. This picker measures the time it takes to select a date and so too does the new date picker. In tests behaviour 1 took about the same time as the old picker but was superior in that the user only needs to use one gesture. Behavior 2 was about 1.4 times faster and behaviour 3 was about 1.5 times faster.

This library also has a setting for angle snapping.

**HGTimeDialer User Instructions:**
This control needs little explanation.

Just use 3 gestures to select a time. The first selects the hour, the second selects the minute and the third the second.

This has three behaviour flags: Cumulative dial, Angle Snapping and Relative Time.

Cumulative dial when disabled; causes the angle of the hands to move directly to the time where the gesture goes down and rotates from there. When cumulative is enabled the hands do not dial until the user rotates/moves the gesture. Note: when disabled touching the hour to the left of the dial will start the hour at 18:00 – 23:00 and touching to the right 00:00 – 06:00.

Angle snapping does exactly that. The hour only snaps between hours but the minutes and seconds can be set to snap to any interval.

Relative Time, when enabled: causes the minute hand to act upon the hour hand and the second hand to act upon the minute.

In tests this library was about 3-4 times faster than the old time pickers.

This repos comes with full documentation and compiled aar libs and signed release demo app.

**Notice: HGDialV2 has landed check it out: https://bitbucket.org/warwick/hg_dial_v2**

1. Enhancements include: The way one dial acts upon another (acting dials) is greatly improved, optimised and very intuitive to the developer.
1. The angle snap now functions intuitively with any angle snap angle (the angle no longer has to be evenly divisible by 1).
1. Overall major optimisations.
1. Better separation of concerns.
1. Minimised lines of code.
1. Added new usages (Can now add arrays of dial objects).
1. Added save/restore and flush state objects.
1. Can now interact with multiple dials at the same time.
1. Works fluidly with device rotation.

**LICENSE**

This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2016, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1.     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
1.     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.