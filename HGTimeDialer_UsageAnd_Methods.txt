Usage Instructions XML Layout
<FrameLayout
	android:id="@+id/hgDateDialerContainer"
	android:layout_width="wrap_content"
	android:layout_height="wrap_content"
	android:clipChildren="false"
	android:layout_below="@+id/lblTimeStatus"
	android:layout_marginTop="20dp"
	android:layout_centerHorizontal="true">

	<ImageView
		android:id="@+id/imageView"
		android:layout_width="280dp"
		android:layout_height="280dp"
		app:srcCompat="@drawable/time_bg"/>

	<com.WarwickWestonWright.HGTimeDialer.HGTimeDialer
		android:id="@+id/hgTimeDialer"
		android:layout_height="280dp"
		android:layout_width="280dp"
		app:hourDrawable="@drawable/hour">
	</com.WarwickWestonWright.HGTimeDialer.HGTimeDialer>

	<com.WarwickWestonWright.HGTimeDialer.HGMinuteDialer
		android:id="@+id/hgMinuteDialer"
		android:layout_height="280dp"
		android:layout_width="280dp"
		app:minuteDrawable="@drawable/minute">
	</com.WarwickWestonWright.HGTimeDialer.HGMinuteDialer>

	<com.WarwickWestonWright.HGTimeDialer.HGSecondDialer
		android:id="@+id/hgMSecondDialer"
		android:layout_height="280dp"
		android:layout_width="280dp"
		app:secondDrawable="@drawable/second">
	</com.WarwickWestonWright.HGTimeDialer.HGSecondDialer>

</FrameLayout>

Usage Instructions Java
hgTimeDialer = (HGTimeDialer) findViewById(R.id.hgTimeDialer);
hgMinuteDialer = (HGMinuteDialer) findViewById(R.id.hgMinuteDialer);
hgSecondDialer = (HGSecondDialer) findViewById(R.id.hgMSecondDialer);

//These 2 setters must me called before using any setters from the HGTimeDialer object.
hgTimeDialer.setHgMinuteDialer(hgMinuteDialer);
hgTimeDialer.setHgSecondDialer(hgSecondDialer);
hgTimeDialer.registerCallback(this);

HGTimeDialer Class Notes:
Methods Descriptions and recommendations:
NOTE: All of the public methods not named in this doc are at present untested and experimental. these other public methods are left in as open ends for future use.

Gets the cumulative rotate behavioural flag
boolean getCumulativeRotate()

Gets the hour snapping behavioural flag
boolean getHourSnap()

Retrieves the active hand (Hour, Minute or Second)
int getWhatHand()

Sets the cumulative rotating behavioural flag
void setCumulativeRotate(final boolean cumulativeRotate)

Manually sets what hand will be used with the next gesture input. Without setting this flag the library will default to using the first gesture to change the hour, the second for the minute and third for the second.
void setWhatHand(int whatHand)

Retrieves the libraries’ internal time
String getInternalTime()

Sets the snapping behaviour for the hour hand; causing the hour hand to snap into each hour position when set to true
void setHourSnap(boolean hourSnap)

Sets the internal time for the library; moving the hands to the relative position
public void setInternalTime(String time)

Sets the flag for relative time behaviour. When set to true the minute hand will act upon the hour hand and the second hand will act upon the minute hand.
Note: At present when the second hand acts upon the minute hand it does not subsequently act upon the hour hand. The developer may not bother adding this behaviour as it is too acute to see the action.
void setRelativeTime(boolean relativeTime)


HGMinuteDialer Class Notes:
Sets the cumulative rotating behavioural flag
setCumulativeRotate(final boolean cumulativeRotate)

Gets the cumulative rotating behavioural flag
boolean getCumulativeRotate()

Sets the snapping behaviour (in minutes) for the minute dial
void setMinuteSnap(int minuteSnap)

Gets the value for the minute snapping (in minutes)
int getMinuteSnap()


HGSecondDialer Class Notes:
Sets the snapping behaviour (in seconds) for the second dial
void setSecondSnap(int secondSnap)

Gets the snapping behaviour (in seconds) for the second dial
int getSecondSnap()

Sets the cumulative rotating behavioural flag
setCumulativeRotate(final boolean cumulativeRotate)

Gets the cumulative rotating behavioural flag
boolean getCumulativeRotate()