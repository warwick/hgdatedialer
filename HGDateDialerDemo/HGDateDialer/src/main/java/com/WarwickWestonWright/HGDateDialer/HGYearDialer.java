/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDateDialer;
//This class is just a dummy canvas to use as the client developer wishes and actually doesn't have to be used at all.
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

public class HGYearDialer extends View {

	/* Top of block field declarations */
	private static int contentWidth;
	private static int contentHeight;
	private Drawable yearDrawable;
	private static double yearRelativeSize = 1f / 3f;
	private double yearAngle;
	private Canvas yearCanvas;
	private Point viewCenterPoint;
    /* Bottom of block field declarations */

	public HGYearDialer(Context context) {
		super(context);

		init(null, 0);
		setFields();

	}//End public HGDial(Context context)


	public HGYearDialer(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(attrs, 0);
		setFields();

	}//End public HGDial(Context context, AttributeSet attrs)


	private void setFields() {

		yearAngle = 0;

	}//End private void setFields()


	private void init(AttributeSet attrs, int defStyle) {

		final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HGYearDialer, defStyle, 0);

		if(a.hasValue(R.styleable.HGYearDialer_yearDrawable)) {

			yearDrawable = a.getDrawable(R.styleable.HGYearDialer_yearDrawable);
			yearDrawable.setCallback(this);

		}

		a.recycle();

	}//End private void init(AttributeSet attrs, int defStyle)


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if(viewCenterPoint != null) {

			yearDrawable.draw(canvas);

		}
		else /* if(viewCenterPoint == null) */ {

			this.yearCanvas = canvas;
			contentWidth = getWidth();
			contentHeight = getHeight();

			viewCenterPoint = new Point(contentWidth / 2, contentHeight / 2);
			yearDrawable.setBounds(
				viewCenterPoint.x - (int) ((float) contentWidth * (yearRelativeSize / 2f)),
				viewCenterPoint.y - (int) ((float) contentHeight * (yearRelativeSize / 2f)),
				viewCenterPoint.x + (int) ((float) contentWidth * (yearRelativeSize / 2f)),
				viewCenterPoint.y + (int) ((float) contentHeight * (yearRelativeSize / 2f)));

			if(yearDrawable != null) {

				yearDrawable.draw(canvas);

			}

		}//End if(viewCenterPoint != null)

	}//End protected void onDraw(Canvas canvas)


	public void setYearRelativeSize(double monthRelativeSize) {this.yearRelativeSize = monthRelativeSize;}
	public double getYearRelativeSize() {return this.yearRelativeSize;}
	public Canvas getYearCanvas() {return this.yearCanvas;}
	public Drawable getYearDrawable() {return this.yearDrawable;}

}