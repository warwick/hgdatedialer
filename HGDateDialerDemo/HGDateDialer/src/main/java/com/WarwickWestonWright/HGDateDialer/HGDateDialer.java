/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDateDialer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class HGDateDialer extends View {

	/* Top of block field declarations */
	private OnTouchListener onTouchListener;
	private boolean viewIsSetup = false;
	private int contentWidth;
	private int contentHeight;
	private static IHGDateDial ihgDateDial;
	private final HGDateInfo hgDateInfo = new HGDateInfo();
	private final GetAngleWrapper getAngleWrapper = new GetAngleWrapper();
	private float firstTouchX;
	private float firstTouchY;
	private double storedGestureAngleBaseOne;
	private double currentGestureAngleBaseOne;
	private double fullGestureAngleBaseOne;
	private double distanceFromCenter;
	private double onDownAngleObjectCumulativeBaseOne;
	private double onUpAngleGestureBaseOne;
	private double precisionRotation;
	private double maximumRotation;//Leave in for time being. I may implement a min/max date behaviour.
	private double minimumRotation;//Leave in for time being. I may implement a min/max date behaviour.
	private boolean useMinMaxRotation;//Leave in for time being. I may implement a min/max date behaviour.
	private boolean useAngleSnap;
	private double snapAngle;
	private double lastSnapAngle;
	private boolean dateChanged;
	private int minMaxRotationOutOfBounds;
	private int boxBehaviour;
	public static final int GESTURE_YEAR = 1;
	public static final int GESTURE_MONTH = 2;
	public static final int GESTURE_DATE = 3;
	private int whatDial;//Keeps track of gesture number for out-of-the-box behaviour 2 and 3.
	private long quickTapTime;
	private long gestureDownTime;
	private boolean suppressInvalidate;
	private double variableDialInner;
	private double variableDialOuter;
	private double dateInnerDiameter;
	private double monthInnerDiameter;
	private int imageRadius;
	private double yearRadius;
	private Drawable date28Drawable;
	private Drawable date29Drawable;
	private Drawable date30Drawable;
	private Drawable date31Drawable;
	private double dateRelativeSize;
	private HGMonthDialer hgMonthDialer;
	private HGYearDialer hgYearDialer;
	private final GregorianCalendar gregorianCalendar = new GregorianCalendar(Locale.getDefault());
	private final int[] yyMMdd = new int[3];
	private int activeDateDial;
	private int touchedDial;
	private Canvas dateCanvas;
	private double monthAngle;
    /* Bottom of block field declarations */

	/* Top of block field declarations */
	//This is the main interface that sends state information to the client program/app
	public interface IHGDateDial {

		void onDown(HGDateInfo hgDateInfo);
		void onPointerDown(HGDateInfo hgDateInfo);//Open End. Will Probably Remove
		void onMove(HGDateInfo hgDateInfo);
		void onPointerUp(HGDateInfo hgDateInfo);//Open End. Will Probably Remove
		void onUp(HGDateInfo hgDateInfo);

	}

	//Top of block main angle wrapper used by outer class
	//Class passes the data for internal use regardless of its' public scope
	final public class GetAngleWrapper {

		private final Point viewCenterPoint = new Point(0, 0);
		private int rotationObjectDirection;
		private int rotationObjectCount;
		private double angleObjectBaseOne;
		private double gestureTouchAngle;

		public GetAngleWrapper() {

			this.rotationObjectDirection = 0;
			this.angleObjectBaseOne = 0;
			this.rotationObjectCount = 0;
			this.gestureTouchAngle = 0;

		}//End public GetAngleWrapper()

		/* Accessors */
		public int getObjectRotationDirection() {return this.rotationObjectDirection;}//Not used may be removed when library is complete
		public int getObjectRotationCount() {return this.rotationObjectCount;}//Not used may be removed when library is complete
		public double getObjectAngleBaseOne() {return this.angleObjectBaseOne;}
		public double getGestureTouchAngle() {return this.gestureTouchAngle;}

		/* Mutators */
		private void setObjectRotationDirection(final int rotationObjectDirection) {this.rotationObjectDirection = rotationObjectDirection;}
		private void setObjectRotationCount(final int objectRotationCount) {this.rotationObjectCount = objectRotationCount;}
		private void setObjectAngleBaseOne(final double precisionAngleBaseOne) {this.angleObjectBaseOne = precisionAngleBaseOne;}
		private void setGestureTouchAngle(final double gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}

	}//End final public class GetAngleWrapper


	//Passed through the IHGDateDial interface to the client program/app
	final public class HGDateInfo {

		private boolean quickTap;
		private int objectRotationDirection;
		private final int[] yyMMdd = new int[3];

		public HGDateInfo() {

			this.quickTap = false;
			this.objectRotationDirection = 0;

		}

		/* Accessors */
		public boolean getQuickTap() {return this.quickTap;}//Will most likely be left in for quick tap callback
		public int getObjectRotationDirection() {return this.objectRotationDirection;}
		public int[] getYyMMdd() {return this.yyMMdd;}

		/* Mutators */
		public void setQuickTap(boolean quickTap) {this.quickTap = quickTap;}
		public void setObjectRotationDirection(int objectRotationDirection) {this.objectRotationDirection = objectRotationDirection;}


		public void setYyMMdd(final int[] yyMMdd) {

			this.yyMMdd[0] = yyMMdd[0];
			this.yyMMdd[1] = yyMMdd[1] + 1;
			this.yyMMdd[2] = yyMMdd[2];

		}

	}//End final public class HGDateInfo
	//Bottom of block main angle wrapper used by outer class


	//Boilerplate extended view constructor
	public HGDateDialer(Context context) {
		super(context);

		setOnTouchListener(getOnTouchListerField());
		init(null, 0);
		setFields();

	}//End public HGDial(Context context)


	//Boilerplate extended view constructor
	public HGDateDialer(Context context, AttributeSet attrs) {
		super(context, attrs);

		setOnTouchListener(getOnTouchListerField());
		init(attrs, 0);
		setFields();

	}//End public HGDial(Context context, AttributeSet attrs)


	private void setFields() {

		this.contentWidth = 0;
		this.contentHeight = 0;
		this.ihgDateDial = null;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.storedGestureAngleBaseOne = 0f;
		this.currentGestureAngleBaseOne = 0f;
		this.fullGestureAngleBaseOne = 0f;
		this.distanceFromCenter = 0f;
		this.onDownAngleObjectCumulativeBaseOne = 0f;
		this.onUpAngleGestureBaseOne = 0f;
		this.precisionRotation = 1f;
		this.maximumRotation = 0f;
		this.minimumRotation = 0f;
		this.useMinMaxRotation = false;
		this.useAngleSnap = false;
		this.snapAngle = 0d;
		this.lastSnapAngle = this.snapAngle;
		this.dateChanged = false;
		this.minMaxRotationOutOfBounds = 0;
		this.boxBehaviour = 1;
		this.whatDial = GESTURE_YEAR;
		this.quickTapTime = 100;
		this.gestureDownTime = 100;
		this.suppressInvalidate = false;
		this.variableDialInner = 0f;
		this.variableDialOuter = 0f;
		this.dateInnerDiameter = 0;
		this.monthInnerDiameter = 0;
		this.imageRadius = 0;
		this.yearRadius = 0;
		this.dateRelativeSize = 1d;
		this.gregorianCalendar.setGregorianChange(new Date(Long.MAX_VALUE));
		this.activeDateDial = 31;
		this.touchedDial = 1;

	}//End private void setFields()


	public void registerCallback(final IHGDateDial ihgDateDial) {HGDateDialer.ihgDateDial = ihgDateDial;}


	private void init(AttributeSet attrs, int defStyle) {

		final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HGDateDialer, defStyle, 0);

		//initialise the four date disc drawables picking up images from XML layout
		if(a.hasValue(R.styleable.HGDateDialer_date28Drawable)) {

			date28Drawable = a.getDrawable(R.styleable.HGDateDialer_date28Drawable);
			date28Drawable.setCallback(this);

		}

		if(a.hasValue(R.styleable.HGDateDialer_date29Drawable)) {

			date29Drawable = a.getDrawable(R.styleable.HGDateDialer_date29Drawable);
			date29Drawable.setCallback(this);

		}

		if(a.hasValue(R.styleable.HGDateDialer_date30Drawable)) {

			date30Drawable = a.getDrawable(R.styleable.HGDateDialer_date30Drawable);
			date30Drawable.setCallback(this);

		}

		if(a.hasValue(R.styleable.HGDateDialer_date31Drawable)) {

			date31Drawable = a.getDrawable(R.styleable.HGDateDialer_date31Drawable);
			date31Drawable.setCallback(this);

		}

		a.recycle();

	}//End private void init(AttributeSet attrs, int defStyle)


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if(viewIsSetup == true) {

			if(useAngleSnap == false) {

				canvas.rotate((float) (fullGestureAngleBaseOne * 360), getAngleWrapper.viewCenterPoint.x, getAngleWrapper.viewCenterPoint.y);

			}
			else /* if(useAngleSnap == true) */ {

				canvas.rotate((float) (snapAngle * 360), getAngleWrapper.viewCenterPoint.x, getAngleWrapper.viewCenterPoint.y);

			}//End if(useAngleSnap == false)

			//Choose which date dial to draw through touch events
			if(activeDateDial == 31) {

				date31Drawable.draw(canvas);

			}
			else if(activeDateDial == 30) {

				date30Drawable.draw(canvas);

			}
			else if(activeDateDial == 29) {

				date29Drawable.draw(canvas);

			}
			else if(activeDateDial == 28) {

				date28Drawable.draw(canvas);

			}//End if(activeDateDial == 31)

			hgMonthDialer.setMonthAngle(monthAngle);
			hgMonthDialer.invalidate();

		}
		else /* if(getAngleWrapper.viewCenterPoint == null) */ {

			this.dateCanvas = canvas;
			contentWidth = getWidth();
			contentHeight = getHeight();

			//Set radius used in variable dial algorithm
			if(contentWidth >= contentHeight) {

				imageRadius = (int) ((float) contentHeight / 2f);

			}
			else if(contentWidth < contentHeight) {

				imageRadius = (int) ((float) contentWidth / 2f);

			}//End if(contentWidth >= contentHeight)

			this.dateInnerDiameter = (int) ((contentWidth - (contentWidth * hgYearDialer.getYearRelativeSize())) / 2f);
			this.monthInnerDiameter = (int) ((contentWidth - (contentWidth * hgMonthDialer.getMonthRelativeSize())) / 2f);
			this.yearRadius = imageRadius * hgYearDialer.getYearRelativeSize();

			//dateRelativeSize is to set the proportional width of the date dial in respect to the size of it's containing layout (FrameLayout)
			getAngleWrapper.viewCenterPoint.x = contentWidth / 2;
			getAngleWrapper.viewCenterPoint.y = contentHeight / 2;
			date28Drawable.setBounds(
				getAngleWrapper.viewCenterPoint.x - (int) ((float) contentWidth * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y - (int) ((float) contentHeight * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.x + (int) ((float) contentWidth * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y + (int) ((float) contentHeight * (dateRelativeSize / 2f)));
			date29Drawable.setBounds(
				getAngleWrapper.viewCenterPoint.x - (int) ((float) contentWidth * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y - (int) ((float) contentHeight * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.x + (int) ((float) contentWidth * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y + (int) ((float) contentHeight * (dateRelativeSize / 2f)));
			date30Drawable.setBounds(
				getAngleWrapper.viewCenterPoint.x - (int) ((float) contentWidth * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y - (int) ((float) contentHeight * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.x + (int) ((float) contentWidth * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y + (int) ((float) contentHeight * (dateRelativeSize / 2f)));
			date31Drawable.setBounds(
				getAngleWrapper.viewCenterPoint.x - (int) ((float) contentWidth * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y - (int) ((float) contentHeight * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.x + (int) ((float) contentWidth * (dateRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y + (int) ((float) contentHeight * (dateRelativeSize / 2f)));

			//Execute initial rendering when library is instantiated
			if(activeDateDial == 31) {

				if(date31Drawable != null) {

					date31Drawable.draw(canvas);

				}

			}
			else if(activeDateDial == 30) {

				if(date30Drawable != null) {

					date30Drawable.draw(canvas);

				}

			}
			else if(activeDateDial == 29) {

				if(date29Drawable != null) {

					date29Drawable.draw(canvas);

				}

			}
			else if(activeDateDial == 28) {

				if(date28Drawable != null) {

					date28Drawable.draw(canvas);

				}

			}//End if(activeDateDial == 31)

			viewIsSetup = true;
			if(hgYearDialer != null) {hgYearDialer.invalidate();}
			if(hgMonthDialer != null) {hgMonthDialer.invalidate();}
			invalidate();

		}//End if(viewIsSetup == true)

	}//End protected void onDraw(Canvas canvas)


	public Canvas getDateCanvas() {return this.dateCanvas;}
	public Drawable getDate28Drawable() {return this.date28Drawable;}
	public Drawable getDate29Drawable() {return this.date29Drawable;}
	public Drawable getDate30Drawable() {return this.date30Drawable;}
	public Drawable getDate31Drawable() {return this.date31Drawable;}

	public void setDate28Drawable(final Drawable date28Drawable) {this.date28Drawable = date28Drawable;}
	public void setDate29Drawable(final Drawable date29Drawable) {this.date29Drawable = date29Drawable;}
	public void setDate30Drawable(final Drawable date30Drawable) {this.date30Drawable = date30Drawable;}
	public void setDate31Drawable(final Drawable date31Drawable) {this.date30Drawable = date31Drawable;}
	public void setDialRelativeSizes(final float dateRelativeSize) {this.dateRelativeSize = dateRelativeSize;}

	/* Top of block touch methods */
	private void setDownTouch(final MotionEvent event) {

		try {

			firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0)));
			firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0)));

		}
		catch(IndexOutOfBoundsException e) {

			return;

		}

	}//End private void setDownTouch(final MotionEvent event)


	private void setMoveTouch(final MotionEvent event) {

		try {

			firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0)));
			firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0)));

		}
		catch(IndexOutOfBoundsException e) {

			return;

		}

	}//End private void setMoveTouch(final MotionEvent event)


	private void setUpTouch(final MotionEvent event) {

		try {

			firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0)));
			firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0)));

		}
		catch(IndexOutOfBoundsException e) {

			return;

		}

	}//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


	/* Top of block rotate functions */
	private HGDateInfo doDownDial() {

		distanceFromCenter = getTwoFingerDistance(getAngleWrapper.viewCenterPoint.x, getAngleWrapper.viewCenterPoint.y, firstTouchX, firstTouchY);
		getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) firstTouchX, (int) firstTouchY)));
		onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;
		fullGestureAngleBaseOne = fullGestureAngleBaseOne * precisionRotation;
		setReturnType();
		return hgDateInfo;

	}//End private HGResult doDownDial()


	private HGDateInfo doMoveDial() {

		getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) firstTouchX, (int) firstTouchY)));
		setReturnType();

		return hgDateInfo;

	}//End private HGResult doMoveDial()


	private HGDateInfo doUpDial() {

		getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) firstTouchX, (int) firstTouchY)));
		onUpAngleGestureBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle())) % 1;
		precisionRotation = 1;

		if(hgDateInfo.getQuickTap() == false) {

			//Steps through gesture numbers 1 - 3 for out-of-the-box behaviours 2 and 3 (behaviour 1 uses only a single gesture to select entire date)
			if(boxBehaviour != 1) {

				if(whatDial == GESTURE_YEAR) {

					whatDial = GESTURE_MONTH;

					if(boxBehaviour == 3) {

						hgMonthDialer.bringToFront();

					}

				}
				else if(whatDial == GESTURE_MONTH) {

					whatDial = GESTURE_DATE;

					if(boxBehaviour == 3) {

						bringToFront();

					}

				}
				else if(whatDial == GESTURE_DATE) {

					whatDial = GESTURE_YEAR;

					if(boxBehaviour == 3) {

						hgYearDialer.bringToFront();

					}

				}//End if(whatDial == GESTURE_YEAR)

			}//End if(boxBehaviour != 1)

			setReturnType();

		}//End if(hgDateInfo.getQuickTap() == false)

		return hgDateInfo;

	}//End private HGResult doUpDial()
	/* Bottom of block rotate functions */


	/* Top of block main functions */
	private void setReturnType() {

		hgDateInfo.setObjectRotationDirection(setVariableDialAngleAndReturnDirection());

		if(suppressInvalidate == false) {

			invalidate();

		}//End if(suppressInvalidate == false)

	}//End private void setReturnType()


	private int setVariableDialAngleAndReturnDirection() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final double angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne) % 1;
		double variablePrecision = getVariablePrecision();

		//Detect direction of rotation and update fullGestureAngleBaseOne which is used for the angle of the onDraw
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;

				if(minMaxRotationOutOfBounds == 0) {

					fullGestureAngleBaseOne -= ((angleDifference * variablePrecision));

				}

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;

				if(minMaxRotationOutOfBounds == 0) {

					fullGestureAngleBaseOne += -(angleDifference * variablePrecision);

				}

			}

			if(variablePrecision < 0) {

				getAngleWrapper.setObjectRotationDirection(-returnValue[0]);

			}
			else if(variablePrecision > 0) {

				getAngleWrapper.setObjectRotationDirection(returnValue[0]);

			}
			else {

				getAngleWrapper.setObjectRotationDirection(0);

			}//End if(variablePrecision < 0)

		}//End if(!(Math.abs(angleDifference) > 0.75f))

		//Detect Year Change and constrain fullGestureAngleBaseOne to have a value ranging from 0f - under 12f
		if(fullGestureAngleBaseOne > 12) {

			fullGestureAngleBaseOne = fullGestureAngleBaseOne % 12;
			yyMMdd[0]++;

		}
		else if(fullGestureAngleBaseOne < 0) {

			fullGestureAngleBaseOne += 12;
			yyMMdd[0]--;

		}

		if(fullGestureAngleBaseOne < 0) {

			fullGestureAngleBaseOne += 12;

		}
		else /* if(fullGestureAngleBaseOne >= 0f) */ {

			fullGestureAngleBaseOne = fullGestureAngleBaseOne % 12;

		}//End if(fullGestureAngleBaseOne < 0f)

		storedGestureAngleBaseOne = currentGestureAngleBaseOne;
		setInternalDate();

		/* May re-purpose this code to have minimum/maximum date constraint */

		hgDateInfo.setYyMMdd(yyMMdd);
		return returnValue[0];//Return the rotation direction -1 for counterclockwise and 1 for clockwise

	}//End private int setVariableDialAngleAndReturnDirection()


	private void setInternalDate() {

		//Main algorithm to calculate the angles to return the correct dates
		final int fullRotationCount = (int) fullGestureAngleBaseOne;

		if(useAngleSnap == true) {

			monthAngle = (int) fullGestureAngleBaseOne / 12d;

			if(lastSnapAngle == snapAngle) {

				dateChanged = false;//Handle not yet implemented;

			}
			else if(lastSnapAngle != snapAngle) {

				dateChanged = true;//Handle not yet implemented;
				lastSnapAngle = snapAngle;

			}//End if(lastSnapAngle == snapAngle)

		}
		else {

			monthAngle = fullGestureAngleBaseOne / 12;

		}//End if(useAngleSnap == true)

		yyMMdd[1] = (int) (monthAngle * 12);

		if(fullRotationCount != 3 && fullRotationCount != 5 && fullRotationCount != 8 && fullRotationCount != 10 && fullRotationCount != 1) {

			yyMMdd[2] = (int) ((fullGestureAngleBaseOne % 1) * 31) + 1;
			activeDateDial = 31;

		}
		else if(fullRotationCount == 3 || fullRotationCount == 5 || fullRotationCount == 8 || fullRotationCount == 10) {

			yyMMdd[2] = (int) ((fullGestureAngleBaseOne % 1) * 30) + 1;
			activeDateDial = 30;

		}
		else if(fullRotationCount == 1) {

			if(yyMMdd[0] % 4 == 0) {

				yyMMdd[2] = (int) ((fullGestureAngleBaseOne % 1) * 29) + 1;
				activeDateDial = 29;

			}
			else /* if(yyMMdd[0] % 4 != 0) */ {

				yyMMdd[2] = (int) ((fullGestureAngleBaseOne % 1) * 28) + 1;
				activeDateDial = 28;

			}//End if(yyMMdd[0] % 4 == 0)

		}//End if(fullRotationCount != 3 && fullRotationCount != 5 && fullRotationCount != 8 && fullRotationCount != 10 && fullRotationCount != 1)

	}//End private void setInternalDate()


	//Main algorithm to dynamically change the rate of rotation in response to the distance from the centre of the dial
	private double getVariablePrecision() {

		if(useAngleSnap == true) {lastSnapAngle = snapAngle;}
		distanceFromCenter = getTwoFingerDistance(getAngleWrapper.viewCenterPoint.x, getAngleWrapper.viewCenterPoint.y, firstTouchX, firstTouchY);
		double[] variablePrecision = new double[1];

		if(boxBehaviour == 1) {

			if(useAngleSnap == true) {snapAngle = ((1d / activeDateDial) * (yyMMdd[2] - 1)) + (yyMMdd[2] / activeDateDial);}

			//Touching the date part of the dial
			if(distanceFromCenter > imageRadius - monthInnerDiameter) {

				touchedDial = 3;
				variablePrecision[0] = 1;

			}
			//Touching the month part of the dial
			else if(distanceFromCenter > imageRadius - dateInnerDiameter) {

				touchedDial = 2;
				variablePrecision[0] = 12;

			}
			//Touching the year part of the dial
			else {

				touchedDial = 1;
				variablePrecision[0] = (variableDialInner - (((distanceFromCenter / yearRadius) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;

			}//End if(distanceFromCenter > imageRadius - monthInnerDiameter)

		}
		else if(boxBehaviour == 2) {

			if(useAngleSnap == true) {snapAngle = ((1d / activeDateDial) * (yyMMdd[2] - 1)) + (yyMMdd[2] / activeDateDial);}

			if(whatDial == GESTURE_YEAR) {

				//Pick Year
				if(distanceFromCenter <= imageRadius) {

					//snapAngle = 1d;
					touchedDial = 1;
					variablePrecision[0] = (variableDialInner - (((distanceFromCenter / imageRadius) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;

				}

			}
			else if(whatDial == GESTURE_MONTH) {

				//Pick Month
				touchedDial = 2;
				if(distanceFromCenter <= imageRadius) {variablePrecision[0] = 12;}

			}
			else if(whatDial == GESTURE_DATE) {

				//Pick Date
				//snapAngle = ((1d / (double) activeDateDial) * (yyMMdd[2] - 1));
				touchedDial = 3;
				if(distanceFromCenter <= imageRadius) {variablePrecision[0] = 1;}

			}//End if(whatDial == 1)

		}
		else if(boxBehaviour == 3) {

			if(useAngleSnap == true) {snapAngle = ((1d / activeDateDial) * (yyMMdd[2] - 1)) + (yyMMdd[2] / activeDateDial);}

			if(whatDial == GESTURE_YEAR) {

				//Pick Year
				if(distanceFromCenter <= imageRadius) {

					//snapAngle = 1d;
					touchedDial = 1;
					variablePrecision[0] = (variableDialInner - (((distanceFromCenter / imageRadius) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;

				}

			}
			else if(whatDial == GESTURE_MONTH) {

				//Pick Month
				touchedDial = 2;
				if(distanceFromCenter <= imageRadius) {variablePrecision[0] = 12;}

			}
			else if(whatDial == GESTURE_DATE) {

				//Pick Date
				//snapAngle = ((1d / (double) activeDateDial) * (yyMMdd[2] - 1));
				touchedDial = 3;
				if(distanceFromCenter <= imageRadius) {variablePrecision[0] = 1;}

			}//End if(whatDial == GESTURE_YEAR)

		}//End if(boxBehaviour == 1)

		return variablePrecision[0];

	}//End private double getVariablePrecision()
    /* Bottom of block main functions */


	/* Top of block Accessors */
	public long getQuickTapTime() {return this.quickTapTime;}
	public OnTouchListener retrieveLocalOnTouchListener() {return onTouchListener;}
	public boolean getSuppressInvalidate() {return this.suppressInvalidate;}
	public double getVariableDialInner() {return this.variableDialInner;}
	public double getVariableDialOuter() {return this.variableDialOuter;}
	public HGMonthDialer getHgMonthDialer() {return this.hgMonthDialer;}
	public HGYearDialer getHgYearDialer() {return this.hgYearDialer;}
	public Date getDate() {return gregorianCalendar.getTime();}
	public boolean getUseAngleSnap() {return this.useAngleSnap;}
	public int getBoxBehaviour() {return this.boxBehaviour;}
	public int getWhatDial() {return this.whatDial;}
	public int getTouchedDial() {return this.touchedDial;}
    /* Bottom of block Accessors */


	/* Top of block Mutators */
	public void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
	public void setSuppressInvalidate(final boolean suppressInvalidate) {this.suppressInvalidate = suppressInvalidate;}
	public void setHgMonthDialer(final HGMonthDialer hgMonthDialer) {this.hgMonthDialer = hgMonthDialer;}
	public void setHgYearDialer(final HGYearDialer hgYearDialer) {this.hgYearDialer = hgYearDialer;}

	public void setBoxBehaviour(final int boxBehaviour, final double monthRelativeSize, final double yearRelativeSize) {

		this.boxBehaviour = boxBehaviour;
		this.whatDial = GESTURE_YEAR;

		if(boxBehaviour == 3) {

			hgMonthDialer.setMonthRelativeSize(1d);//For out-of-the-box behaviour 3
			hgYearDialer.setYearRelativeSize(1d);//For out-of-the-box behaviour 3

		}
		else if(boxBehaviour == 2 || boxBehaviour == 1) {

			hgMonthDialer.setMonthRelativeSize(monthRelativeSize);
			hgYearDialer.setYearRelativeSize(yearRelativeSize);

		}

	}//End public void setBoxBehaviour(final int boxBehaviour, final double monthRelativeSize, final double yearRelativeSize)


	public void setWhatDial(final int whatDial) {

		this.whatDial = whatDial;

		if(boxBehaviour == 3) {

			if(whatDial == 1) {

				hgYearDialer.bringToFront();

			}
			else if(whatDial == 2) {

				hgMonthDialer.bringToFront();

			}
			else if(whatDial == 3) {

				bringToFront();

			}//End if(whatDial == 1)

		}//End if(boxBehaviour == 3)

	}//End public void setWhatDial(final int whatDial)

	//Sets libraries internal date and rotates the dial to parsed date.
	//Developer currently plans on calling this function internally from the onUp of the touch event as onMove would take up too much processor overhead
	public void setInternalDate(final Date date) {

		gregorianCalendar.setTime(date);
		yyMMdd[0] = gregorianCalendar.get(GregorianCalendar.YEAR);
		yyMMdd[1] = gregorianCalendar.get(GregorianCalendar.MONTH);
		yyMMdd[2] = gregorianCalendar.get(GregorianCalendar.DATE);

		if(yyMMdd[1] != 3 && yyMMdd[1] != 5 && yyMMdd[1] != 8 && yyMMdd[1] != 10 && yyMMdd[1] != 1) {

			activeDateDial = 31;
			fullGestureAngleBaseOne = yyMMdd[1] + ((yyMMdd[2] - 1) * (1d / 31));

		}
		else if(yyMMdd[1] == 3 || yyMMdd[1] == 5 || yyMMdd[1] == 8 || yyMMdd[1] == 10) {

			activeDateDial = 30;
			fullGestureAngleBaseOne = yyMMdd[1] + ((yyMMdd[2] - 1) * (1d / 30));

		}
		else if(yyMMdd[1] == 1) {

			if(yyMMdd[0] % 4 != 0) {

				activeDateDial = 28;
				fullGestureAngleBaseOne = yyMMdd[1] + ((yyMMdd[2] - 1) * (1d / 28));

			}
			else if(yyMMdd[0] % 4f == 0) {

				activeDateDial = 29;
				fullGestureAngleBaseOne = yyMMdd[1] + ((yyMMdd[2] - 1) * (1d / 29));

			}//End if(yyMMdd[0] % 4 != 0)

		}//End if(yyMMdd[1] != 3 && yyMMdd[1] != 5 && yyMMdd[1] != 8 && yyMMdd[1] != 10 && yyMMdd[1] != 1)

		setInternalDate();
		if(useAngleSnap == true) {getVariablePrecision();}
		invalidate();

	}//End public void setInternalDate(final Date date)


	public void setVariableDial(final double variableDialInner, final double variableDialOuter) {

		this.variableDialInner = variableDialInner;
		this.variableDialOuter = variableDialOuter;

	}//End public void setVariableDial(final double variableDialInner, final double variableDialOuter)

	//Not implemented yet. Developer has not finalised how this will be implemented if implemented at all
	public void setMinMaxDial(final float minimumRotation, final float maximumRotation, boolean useMinMaxRotation) {

		this.minimumRotation = minimumRotation;
		this.maximumRotation = maximumRotation;
		this.useMinMaxRotation = useMinMaxRotation;

		if(useMinMaxRotation == true) {

			if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation) {

				this.useMinMaxRotation = false;

			}
			else {

				this.useMinMaxRotation = true;

			}//End if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation)

		}//End if(useMinMaxRotation == true)

	}//End public void setMinMaxDial(final float minimumRotation, final float maximumRotation, boolean useMinMaxRotation)

	public void setUseAngleSnap(final boolean useAngleSnap) {this.useAngleSnap = useAngleSnap;}
    /* Bottom of block Mutators */


	/* Top of block convenience methods */
	public double getGestureFullAngle() {return this.fullGestureAngleBaseOne;}
	private void doManualObjectDialInternal(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}


	//Used to manually change date on dial.
	//This method is fully tested and will probably not change.
	public void doManualGestureDial(final double manualDial) {

		fullGestureAngleBaseOne = manualDial;
		getAngleWrapper.setObjectRotationCount((int) (manualDial * precisionRotation));
		getAngleWrapper.setObjectAngleBaseOne(Math.round((getAngleWrapper.getObjectAngleBaseOne()) * 1000000.0f) / 1000000.0f);
		onDownAngleObjectCumulativeBaseOne = 0;
		onUpAngleGestureBaseOne = 0;

		if(suppressInvalidate == false) {

			invalidate();

		}

	}//End public void doManualGestureDial(final double manualDial)


	public void doManualObjectDial(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}
    /* Bottom of block convenience methods */


	//Not currently used
	public void performQuickTap() {

		hgDateInfo.setQuickTap(true);

		post(new Runnable() {
			@Override
			public void run() {

				ihgDateDial.onUp(doUpDial());
				hgDateInfo.setQuickTap(false);

			}
		});

	}//End public void performQuickTap()


	/* Top of block geometry functions */
	public float getAngleFromPoint(final Point centerPoint, final Point touchPoint) {

		float returnVal = 0;

		//+0 - 0.5
		if(touchPoint.x > centerPoint.x) {

			returnVal = (float) (Math.atan2((touchPoint.x - centerPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI);

		}
		//+0.5
		else if(touchPoint.x < centerPoint.x) {

			returnVal = (float)  (1 - (Math.atan2((centerPoint.x - touchPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI));

		}//End if(touchPoint.x > centerPoint.x)

		return returnVal;

	}//End public float getAngleFromPoint(final Point centerPoint, final Point touchPoint)


	public Point getPointFromAngle(final double angle) {return getPointFromAngle(angle, getAngleWrapper.viewCenterPoint.x);}


	public Point getPointFromAngle(final double angle, final double radius) {

		final Point coords = new Point();
		coords.x = (int) (radius - (radius * Math.cos((angle - 0.75) * 2 * Math.PI)));
		coords.y = (int) (radius - (radius * Math.sin((angle - 0.75) * 2 * Math.PI)));

		return coords;

	}//End public Point getPointFromAngle(final double angle, final double radius)


	//Calculate diagonal distance between two points
	public static double getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY) {

		double pinchDistanceX = 0;
		double pinchDistanceY = 0;

		if(firstTouchX > secondTouchX) {

			pinchDistanceX = Math.abs(secondTouchX - firstTouchX);

		}
		else if(firstTouchX < secondTouchX) {

			pinchDistanceX = Math.abs(firstTouchX - secondTouchX);

		}//End if(firstTouchX > secondTouchX)

		if(firstTouchY > secondTouchY) {

			pinchDistanceY = Math.abs(secondTouchY - firstTouchY);

		}
		else if(firstTouchY < secondTouchY) {

			pinchDistanceY = Math.abs(firstTouchY - secondTouchY);

		}//End if(firstTouchY > secondTouchY)

		if(pinchDistanceX == 0 && pinchDistanceY == 0) {

			return  0;

		}
		else {

			pinchDistanceX = (pinchDistanceX * pinchDistanceX);
			pinchDistanceY = (pinchDistanceY * pinchDistanceY);
			return Math.abs(Math.sqrt(pinchDistanceX + pinchDistanceY));

		}//End if(pinchDistanceX == 0 && pinchDistanceY == 0)

	}//End public static double getTwoFingerDistance(final float firstTouchX, final float firstTouchY, final float secondTouchX, final float secondTouchY)
    /* Bottom of block geometry functions */


	public void resetHGDateDial() {setFields();invalidate();}


	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		viewIsSetup = false;

	}


	@Override
	public boolean onTouchEvent(MotionEvent event) {

		sendTouchEvent(this, event);

		return true;

	}


	private OnTouchListener getOnTouchListerField() {

		onTouchListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				hgTouchEvent(v, event);

				return true;

			}

		};

		return onTouchListener;

	}//End private OnTouchListener getOnTouchListerField()


	//Convenience method inject live event into the library from any view in the client app
	public void sendTouchEvent(final View v, final MotionEvent event) {hgTouchEvent(v, event);}


	//Touch Event Handler
	private void hgTouchEvent(final View v, final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		//Top of block used for quick tap
		if(event.getAction() == MotionEvent.ACTION_DOWN) {

			hgDateInfo.setQuickTap(false);
			gestureDownTime = System.currentTimeMillis();

		}
		else if(event.getAction() == MotionEvent.ACTION_UP) {

			//Used for quick tap
			if(System.currentTimeMillis() < gestureDownTime + quickTapTime) {

				hgDateInfo.setQuickTap(true);
				ihgDateDial.onUp(doUpDial());
				return;

			}
			else {

				hgDateInfo.setQuickTap(false);

			}//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

		}//End if(event.getAction() == MotionEvent.ACTION_DOWN)
		//Bottom of block used for quick tap

		switch(action) {

			case MotionEvent.ACTION_DOWN: {

				setDownTouch(event);
				ihgDateDial.onDown(doDownDial());

				break;

			}
			case MotionEvent.ACTION_POINTER_DOWN: {

				//Open End. Will Probably Remove
				setDownTouch(event);
				ihgDateDial.onDown(doDownDial());

				break;

			}
			case MotionEvent.ACTION_MOVE: {

				setMoveTouch(event);
				ihgDateDial.onMove(doMoveDial());

				break;

			}
			case MotionEvent.ACTION_POINTER_UP: {

				//Open End. Will Probably Remove
				setUpTouch(event);
				ihgDateDial.onUp(doUpDial());

				break;

			}
			case MotionEvent.ACTION_UP: {

				setUpTouch(event);
				ihgDateDial.onUp(doUpDial());

				break;

			}
			default:

				break;

		}//End switch(action)

	}//End private void hgTouchEvent(final View v, final MotionEvent event)

}