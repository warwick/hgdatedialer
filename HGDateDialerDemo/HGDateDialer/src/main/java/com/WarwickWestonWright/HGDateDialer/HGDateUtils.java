/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGDateDialer;
//Though this class is protected by open source,
//within reason the developer will allow certain codes to be copied as they are so generic that they may not be reasonably protected by this licence.
import android.content.Context;
import android.text.format.DateFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class HGDateUtils {

	private Context context;
	private static SimpleDateFormat sdf;
	private GregorianCalendar gregorianCalendar;
	private Locale locale;
	private int yearValue;
	private int monthValue;
	private int dateValue;

	public HGDateUtils(Context context) {

		this.context = context;
		locale = new Locale(Locale.getDefault().getLanguage(), Locale.getDefault().getCountry());
		gregorianCalendar = new GregorianCalendar(Locale.getDefault());
		gregorianCalendar.setGregorianChange(new Date(Long.MAX_VALUE));
		yearValue = 0;
		monthValue = 0;
		dateValue = 0;

	}


	public String getDate() {return getDate(getDateFormatForCurrentLocale());}


	public String getDate(String dateFormat) {

		sdf = new SimpleDateFormat(dateFormat);
		return sdf.format(gregorianCalendar.getTime());

	}


	public String getTime() {

		gregorianCalendar = new GregorianCalendar(locale);
		gregorianCalendar.setGregorianChange(new Date(Long.MAX_VALUE));

		String returnValue = Integer.toString(gregorianCalendar.get(Calendar.HOUR_OF_DAY)) + ":" +
			Integer.toString(gregorianCalendar.get(Calendar.MINUTE)) + ":" +
			Integer.toString(gregorianCalendar.get(Calendar.SECOND));
		return returnValue;

	}


	public Date getInternalDate() {return gregorianCalendar.getTime();}


	public GregorianCalendar getGregorianCalendar() {return this.gregorianCalendar;}


	public void setInternalDate(String dateString) {

		setInternalDate(dateString, getDateFormatForCurrentLocale());

	}


	public int getYearValue() {return this.yearValue;}
	public int getMonthValue() {return this.monthValue;}
	public int getDateValue() {return this.dateValue;}


	public boolean setInternalDate(String dateString, String dateFormat) {

		boolean isRealDate = isRealDate(dateString, dateFormat);

		if(isRealDate == true) {

			String[] dateStringNumbers = dateString.split("\\D+");
			String[] dateFormatLetters = dateFormat.split("[^yyyy|^yy|^MM|^dd]");

			//Get Year Value
			if(dateFormatLetters[0].toUpperCase().contains("Y")) {

				yearValue = Integer.parseInt(dateStringNumbers[0]);

			}
			else if(dateFormatLetters[1].toUpperCase().contains("Y")) {

				yearValue = Integer.parseInt(dateStringNumbers[1]);

			}
			else if(dateFormatLetters[2].toUpperCase().contains("Y")) {

				yearValue = Integer.parseInt(dateStringNumbers[2]);

			}//End if(dateFormatLetters[0].toUpperCase().contains("Y"))

			//Get Month Value
			if(dateFormatLetters[0].toUpperCase().contains("M")) {

				monthValue = Integer.parseInt(dateStringNumbers[0]);

			}
			else if(dateFormatLetters[1].toUpperCase().contains("M")) {

				monthValue = Integer.parseInt(dateStringNumbers[1]);

			}
			else if(dateFormatLetters[2].toUpperCase().contains("M")) {

				monthValue = Integer.parseInt(dateStringNumbers[2]);

			}//End if(dateFormatLetters[0].toUpperCase().contains("M"))

			//Get Date Value
			if(dateFormatLetters[0].toUpperCase().contains("D")) {

				dateValue = Integer.parseInt(dateStringNumbers[0]);

			}
			else if(dateFormatLetters[1].toUpperCase().contains("D")) {

				dateValue = Integer.parseInt(dateStringNumbers[1]);

			}
			else if(dateFormatLetters[2].toUpperCase().contains("D")) {

				dateValue = Integer.parseInt(dateStringNumbers[2]);

			}//End if(dateFormatLetters[0].toUpperCase().contains("D"))

			gregorianCalendar.set(yearValue, monthValue - 1, dateValue);

		}//End if(isRealDate == true)

		return isRealDate;

	}//End public boolean setInternalDate(String dateString, String dateFormat)


	//Checks for valid date based upon default/device locale
	public boolean isDateValid(String dateString, Context context) {

		dateString = dateString.replaceAll("\\D+", "-");
		sdf = new SimpleDateFormat(getDateFormatForCurrentLocale());

		try {

			sdf.parse(dateString);
			return true;

		}
		catch (ParseException e) {

			e.printStackTrace();
			return false;

		}

	}//End public boolean isDateValid(String dateString, Context context)


	public String getDateFormatForCurrentLocale() {

		String returnValue = String.copyValueOf(DateFormat.getDateFormatOrder(context));

		returnValue = returnValue.replaceAll("[y]+", "yyyy-");
		returnValue = returnValue.replaceAll("[M]+", "MM-");
		returnValue = returnValue.replaceAll("[d]+", "dd-");
		returnValue = returnValue.substring(0, returnValue.lastIndexOf("-"));

		return returnValue;

	}//End public String getDateFormatForCurrentLocale()


	//Checks for invalid numbers ie month 12, date 32 or year with string length of anything other than 2 or 4
	public boolean isRealDate(String dateString, String dateFormat) {

		String[] dateStringNumbers = dateString.split("\\D+");
		String[] dateFormatLetters = dateFormat.split("[^yyyy|^yy|^MM|^dd]");
		int yearValue = 0; int monthValue = 0; int dateValue = 0;

		//Ensure both date format string and the date itself is divided into 3 parts
		if(dateStringNumbers.length != 3 || dateFormatLetters.length != 3) {

			return false;

		}
		else /* if(dateStringNumbers.length == 3 && dateFormatLetters.length == 3) */ {

			//Load 3 date values into array for evaluation.
			int[] dateIntNumbers = new int[3];

			try {

				for(int i = 0; i < 3; i++) {

					dateIntNumbers[i] = Integer.parseInt(dateStringNumbers[i]);

					if(dateIntNumbers[i] < 1) {return false;}

				}

			}
			catch(NumberFormatException  e) {

				return false;

			}

			for(int i = 0; i < 3; i++) {

				if(dateFormatLetters[i].equals("yy") || dateFormatLetters[i].equals("yyyy")) {

					if(dateStringNumbers[i].length() == 2 || dateStringNumbers[i].length() == 4) {

						yearValue = dateIntNumbers[i];

					}
					else {

						return false;

					}

				}
				else if(dateFormatLetters[i].equals("MM")) {

					monthValue = dateIntNumbers[i];

				}
				else if(dateFormatLetters[i].equals("dd")) {

					dateValue = dateIntNumbers[i];

				}
				else {

					return false;

				}//End if(dateFormatLetters[i].equals("yy") || dateFormatLetters[i].equals("yyyy"))

			}//End for(int i = 0; i < 3; i++)

			if(monthValue > 12) {return false;}
			if(dateValue > 31) {return false;}

			//Not February
			if(monthValue != 2) {

				if((monthValue == 4 || monthValue == 6 || monthValue == 9 || monthValue == 11) && dateValue > 30) {

					return false;

				}

			}
			//Is February
			else if(monthValue == 2) {

				if(yearValue % 4 == 0) {

					if(dateValue > 29) {return false;}

				}
				else {

					if(dateValue > 28) {return false;}

				}

			}//End if(monthValue != 2)

			return true;

		}//End if(dateNumbers.length != 3)

	}//End public boolean isRealDate(String dateString, String dateFormat)

}