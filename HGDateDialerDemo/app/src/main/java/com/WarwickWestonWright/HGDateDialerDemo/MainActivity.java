package com.WarwickWestonWright.HGDateDialerDemo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.WarwickWestonWright.HGDateDialer.HGDateUtils;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.IDatePickerDialog {

	//Declare Views and Activities
	private DatePickerDialog datePickerDialog;
	private Intent dateActivityIntent;
	private Intent timeActivityIntent;

	//Declare Widgets
	private Button btnTestOldDatePicker;
	private Button btnTestNewDatePicker;
	private Button btnTestNewTimePicker;

	//Declare Primitives
	private long datePickStartTime;
	private long datePickDuration;

	//Declare Custom Objects
	private HGDateUtils hgDateUtils;

	private SharedPreferences sp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_activity);
		getSupportActionBar().hide();
		sp = PreferenceManager.getDefaultSharedPreferences(this);

		if(sp.getBoolean("spIsSetup", false) == false) {

			sp.edit().putInt("boxBehaviour", 1).commit();
			sp.edit().putBoolean("dateAngleSnap", true).commit();
			sp.edit().putBoolean("timeAngleSnap", true).commit();
			sp.edit().putInt("timeMinuteSnap", 5).commit();
			sp.edit().putInt("timeSecondSnap", 5).commit();
			sp.edit().putString("minuteSnapTextVal", "5").commit();
			sp.edit().putString("secondSnapTextVal", "5").commit();
			sp.edit().putBoolean("timeCumulativeRotate", true).commit();
			sp.edit().putBoolean("relativeTime", true).commit();
			sp.edit().putBoolean("keepTime", false).commit();

			sp.edit().putBoolean("spIsSetup", true).commit();

		}//End if(sp.getBoolean("spIsSetup", false) == false)

		//Instantiate Custom Objects
		hgDateUtils = new HGDateUtils(this);

		//Instantiate Widgets
		btnTestOldDatePicker = (Button) findViewById(R.id.btnTestOldDatePicker);
		btnTestNewDatePicker = (Button) findViewById(R.id.btnTestNewDatePicker);
		btnTestNewTimePicker = (Button) findViewById(R.id.btnTestNewTimePicker);

		btnTestOldDatePicker.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				Bundle bundle = new Bundle();
				bundle.putString("dateString", hgDateUtils.getDate());
				if(datePickerDialog == null) {datePickerDialog = new DatePickerDialog();}
				datePickerDialog.setCancelable(true);
				datePickerDialog.setArguments(bundle);
				datePickerDialog.show(getSupportFragmentManager().beginTransaction(), "DatePickerDialog");
				datePickStartTime = System.currentTimeMillis();

			}
		});

		btnTestNewDatePicker.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				dateActivityIntent = new Intent(MainActivity.this, DateActivity.class);
				startActivity(dateActivityIntent);

			}
		});

		btnTestNewTimePicker.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				timeActivityIntent =  new Intent(MainActivity.this, TimeActivity.class);
				startActivity(timeActivityIntent);

			}
		});

	}//End protected void onCreate(Bundle savedInstanceState)

	@Override
	public void datePickerDialogCb(int year, int month, int date) {

		datePickDuration = System.currentTimeMillis() - datePickStartTime;
		Toast.makeText(this, "Yout took\n" + Long.toString(datePickDuration / 1000) + "\nSeconds to pick date", Toast.LENGTH_LONG).show();
		datePickerDialog.dismiss();

	}

}