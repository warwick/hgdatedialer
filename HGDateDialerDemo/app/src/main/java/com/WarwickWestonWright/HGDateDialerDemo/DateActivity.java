/*
This Class represents a reference app and is for developers to get a head start with the HGDateDialer.
Though the HGDateDialer is protected by open source, this class is not and is free source to modify and distribute freely without any obligation to the developer.
*/
package com.WarwickWestonWright.HGDateDialerDemo;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.WarwickWestonWright.HGDateDialer.HGDateDialer;
import com.WarwickWestonWright.HGDateDialer.HGDateUtils;
import com.WarwickWestonWright.HGDateDialer.HGMonthDialer;
import com.WarwickWestonWright.HGDateDialer.HGYearDialer;

public class DateActivity extends AppCompatActivity implements HGDateDialer.IHGDateDial {

	//Declare Layouts
	private HGDateDialer hgDateDialer;
	private HGMonthDialer hgMonthDialer;
	private HGYearDialer hgYearDialer;
	private LinearLayout lLayoutDialButtons;

	//Declare Custom Objects
	private HGDateUtils hgDateUtils;

	//Declare Java Objects
	private SharedPreferences sp;

	//Declare Widgets
	private Button btnBoxBehaviourToggle;
	private CheckBox chkDateSnapping;
	private Button btnSetDialDate;
	private EditText txtManualDate;
	private TextView lblDateStatus;
	private Button btnYYYY;
	private Button btnMM;
	private Button btnDD;

	//Declare Strings and Primitives
	private String dateString;
	private String dateStatus;
	private long datePickStartTime;
	private long datePickDuration;
	private int touchedDial;

	TypedValue typedValueActive;
	TypedValue typedValue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.date_activity);

		getSupportActionBar().hide();
		sp = PreferenceManager.getDefaultSharedPreferences(this);

		touchedDial = 0;
		typedValueActive = new TypedValue();
		typedValue = new TypedValue();
		getResources().getValue(R.color.btnDialBgColorActive, typedValueActive, true);
		getResources().getValue(R.color.btnDialBgColor, typedValue, true);

		//Top of Section: these next seven lines below are necessary to setup the date dialler control.
		hgDateUtils = new HGDateUtils(this);
		hgDateDialer = (HGDateDialer) findViewById(R.id.hgDateDialer);
		hgMonthDialer = (HGMonthDialer) findViewById(R.id.hgMonthDialer);
		hgYearDialer = (HGYearDialer) findViewById(R.id.hgYearDialer);
		hgDateDialer.setHgMonthDialer(hgMonthDialer);
		hgDateDialer.setHgYearDialer(hgYearDialer);
		hgDateDialer.registerCallback(this);
		//For rest of usage see FrameLayout with id 'hgDateDialerContainer'
		//Bottom of Section: Later the developer of this library intends to simplify the usage of this library to about 2 or three lines.

		hgDateDialer.setUseAngleSnap(sp.getBoolean("dateAngleSnap", true));//Defaults to true if not set.
		hgMonthDialer.setMonthRelativeSize(2d / 3d);//For out-of-the-box behaviour 1 or 2
		hgYearDialer.setYearRelativeSize(1d / 3d);//For out-of-the-box behaviour 1 or 2
		hgDateDialer.setVariableDial(144d, 12d);//Used only for the rotation sensitivity of the year rotation.

		//Initialise Widgets and layouts
		btnBoxBehaviourToggle = (Button) findViewById(R.id.btnBoxBehaviourToggle);
		chkDateSnapping = (CheckBox) findViewById(R.id.chkDateSnapping);
		btnSetDialDate = (Button) findViewById(R.id.btnSetDialDate);
		txtManualDate = (EditText) findViewById(R.id.txtManualDate);
		lblDateStatus = (TextView) findViewById(R.id.lblDateStatus);
		lLayoutDialButtons = (LinearLayout) findViewById(R.id.lLayoutDialButtons);
		btnYYYY = (Button) findViewById(R.id.btnYYYY);
		btnMM = (Button) findViewById(R.id.btnMM);
		btnDD = (Button) findViewById(R.id.btnDD);

		txtManualDate.setHint(hgDateUtils.getDateFormatForCurrentLocale().toUpperCase());
		dateString = hgDateUtils.getDate();
		txtManualDate.setText(dateString);

		if(sp.getInt("boxBehaviour", 1) == 1) {

			btnBoxBehaviourToggle.setText(getString(R.string.btn_lbl_box_behaviour_toggle1));

		}
		else if(sp.getInt("boxBehaviour", 1) == 2) {

			btnBoxBehaviourToggle.setText(getString(R.string.btn_lbl_box_behaviour_toggle2));

		}
		else if(sp.getInt("boxBehaviour", 1) == 3) {

			btnBoxBehaviourToggle.setText(getString(R.string.btn_lbl_box_behaviour_toggle3));

		}//End if(sp.getInt("boxBehaviour", 1) == 1)

		chkDateSnapping.setChecked(sp.getBoolean("dateAngleSnap", true));
		hgDateDialer.setBoxBehaviour(sp.getInt("boxBehaviour", 1), 2d / 3d, 1d / 3d);

		btnBoxBehaviourToggle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(sp.getInt("boxBehaviour", 1) == 1) {

					sp.edit().putInt("boxBehaviour", 2).commit();
					hgDateDialer.setBoxBehaviour(2, 2d / 3d, 1d / 3d);
					btnBoxBehaviourToggle.setText(getString(R.string.btn_lbl_box_behaviour_toggle2));

					//The following two lines of code are only called because this app is dynamically changing the out-of-the-box behaviour from a value other than 3.
					hgMonthDialer.setMonthRelativeSize(2d / 3d);//For out-of-the-box behaviour 1 or 2
					hgYearDialer.setYearRelativeSize(1d / 3d);//For out-of-the-box behaviour 1 or 2
					finish();
					startActivity(getIntent());

				}
				else if(sp.getInt("boxBehaviour", 1) == 2) {

					sp.edit().putInt("boxBehaviour", 3).commit();
					hgDateDialer.setBoxBehaviour(3, 1d, 1d);
					btnBoxBehaviourToggle.setText(getString(R.string.btn_lbl_box_behaviour_toggle3));
					finish();
					startActivity(getIntent());

				}
				else if(sp.getInt("boxBehaviour", 1) == 3) {

					sp.edit().putInt("boxBehaviour", 1).commit();
					hgDateDialer.setBoxBehaviour(1, 2d / 3d, 1d / 3d);
					btnBoxBehaviourToggle.setText(getString(R.string.btn_lbl_box_behaviour_toggle1));

					//The following two lines of code are only called because this app is dynamically changing the out-of-the-box behaviour from a value other than 3.
					hgMonthDialer.setMonthRelativeSize(2d / 3d);//For out-of-the-box behaviour 1 or 2
					hgYearDialer.setYearRelativeSize(1d / 3d);//For out-of-the-box behaviour 1 or 2
					finish();
					startActivity(getIntent());

				}//End if(sp.getInt("boxBehaviour", 1) == 1)

				setActiveButtons();

			}
		});

		chkDateSnapping.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				sp.edit().putBoolean("dateAngleSnap", chkDateSnapping.isChecked()).commit();
				hgDateDialer.setUseAngleSnap(sp.getBoolean("dateAngleSnap", true));//Defaults to true if not set.

			}
		});

		btnSetDialDate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				//Call with value of 1 to set the first gesture to change the date for out-of-the-box behaviour (when not using behaviour 1)
				//Also needed if you want to cancel the date pick action because the library will persist this value.
				hgDateDialer.setWhatDial(1);
				String date = txtManualDate.getText().toString().trim();

				if(hgDateDialer.getBoxBehaviour() == 3 && hgDateDialer.getUseAngleSnap() == true) {

					if(hgDateUtils.isRealDate(date, hgDateUtils.getDateFormatForCurrentLocale())) {

						setInternalDate(date);
						date = "1-1-" + Integer.toString(hgDateUtils.getYearValue());
						setInternalDate(date);

					}
					else {

						Toast.makeText(getBaseContext(), getString(R.string.toast_invalid_date_error), Toast.LENGTH_LONG).show();

					}//End if(hgDateUtils.isRealDate(date, hgDateUtils.getDateFormatForCurrentLocale()))

				}
				else {

					setInternalDate(date);
					setActiveButtons();

				}//End if(hgDateDialer.getBoxBehaviour() == 3 && hgDateDialer.getUseAngleSnap() == true)

				if(hgDateDialer.getBoxBehaviour() != 1) {setActiveButtons();}

			}
		});

		btnYYYY.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				btnYYYY.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));
				btnMM.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				btnDD.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				if(hgDateDialer.getBoxBehaviour() != 1) {hgDateDialer.setWhatDial(1);}

			}
		});

		btnMM.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				btnYYYY.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				btnMM.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));
				btnDD.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				if(hgDateDialer.getBoxBehaviour() != 1) {hgDateDialer.setWhatDial(2);}

			}
		});

		btnDD.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				btnYYYY.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				btnMM.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				btnDD.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));
				if(hgDateDialer.getBoxBehaviour() != 1) {hgDateDialer.setWhatDial(3);}

			}
		});

		//Sets Date to what ever is in the date text control (in this context todays' date)
		btnSetDialDate.performClick();
		setActiveButtons();

	}//End protected void onCreate(Bundle savedInstanceState)


	private void setActiveButtons() {

		btnYYYY.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));
		btnMM.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
		btnDD.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));

	}//End private void setActiveButtons()


	private void setInternalDate(String date) {

		hgDateUtils.setInternalDate(date);

		if(date.equals("") == false) {

			if(hgDateUtils.isRealDate(date, hgDateUtils.getDateFormatForCurrentLocale())) {

				hgDateDialer.setInternalDate(hgDateUtils.getInternalDate());
				lblDateStatus.setText(date);

			}
			else /* if(!(hgDateUtils.isRealDate(date, hgDateUtils.getDateFormatForCurrentLocale()))) */ {

				Toast.makeText(getBaseContext(), getString(R.string.toast_invalid_date_error), Toast.LENGTH_LONG).show();

			}//End if(hgDateUtils.isRealDate(date, hgDateUtils.getDateFormatForCurrentLocale()))

		}
		else if(date.equals("") == true) {

			Toast.makeText(getBaseContext(), getString(R.string.toast_empty_date_error), Toast.LENGTH_LONG).show();

		}//End if(date.equals("") == false)

	}//End private void setInternalDate(String date)


	@Override
	public void onDown(HGDateDialer.HGDateInfo hgDateInfo) {

		dateStatus = Integer.toString(hgDateInfo.getYyMMdd()[2]) + "-" +
			Integer.toString(hgDateInfo.getYyMMdd()[1]) + "-" +
			Integer.toString(hgDateInfo.getYyMMdd()[0]);
		lblDateStatus.setText(dateStatus);

		//This block just sets up the time to time how long it takes to select a date and corresponds to code in the onUp
		if(hgDateDialer.getBoxBehaviour() != 1) {

			if(hgDateDialer.getWhatDial() == 1) {

				datePickStartTime = System.currentTimeMillis();

			}

		}
		else if(hgDateDialer.getBoxBehaviour() == 1) {

			datePickStartTime = System.currentTimeMillis();

		}//End if(hgDateDialer.getBoxBehaviour() != 1)

	}

	@Override
	public void onPointerDown(HGDateDialer.HGDateInfo hgDateInfo) {/* Open End Will Probably Be Removed From Library */}

	@Override
	public void onMove(HGDateDialer.HGDateInfo hgDateInfo) {

		//Displays date status. Will be adding a handle to detect date changed and date snapped event to preserve processor overhead later
		if(hgDateDialer.getBoxBehaviour() == 3 && hgDateDialer.getUseAngleSnap() == true) {

			//Programmatic behaviour to cause out-of-the-box behaviour 3 to snap to Jan 1st of every year when dialling year and first of every month when dialling month
			if(hgDateDialer.getWhatDial() == 3) {

				dateStatus = Integer.toString(hgDateInfo.getYyMMdd()[2]) + "-" +
					Integer.toString(hgDateInfo.getYyMMdd()[1]) + "-" +
					Integer.toString(hgDateInfo.getYyMMdd()[0]);
				lblDateStatus.setText(dateStatus);

			}
			else if(hgDateDialer.getWhatDial() == 2) {

				dateStatus = "1" + "-" +
					Integer.toString(hgDateInfo.getYyMMdd()[1]) + "-" +
					Integer.toString(hgDateInfo.getYyMMdd()[0]);
				lblDateStatus.setText(dateStatus);

			}
			else {

				dateStatus = "1" + "-" +
					"1" + "-" +
					Integer.toString(hgDateInfo.getYyMMdd()[0]);
				lblDateStatus.setText(dateStatus);

			}//End if(hgDateDialer.getWhatDial() == 3)

		}
		else {

			//Top of Block: Update UI when touched dial changes for out-of-the-box behaviour 1.
			if(hgDateDialer.getBoxBehaviour() == 1) {

				if(touchedDial != hgDateDialer.getTouchedDial()) {

					touchedDial = hgDateDialer.getTouchedDial();

					if(touchedDial == 1) {

						btnYYYY.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));
						btnMM.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
						btnDD.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));

					}
					else if(touchedDial == 2) {

						btnYYYY.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
						btnMM.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));
						btnDD.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));

					}
					else if(touchedDial == 3) {

						btnYYYY.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
						btnMM.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
						btnDD.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));

					}//End if(touchedDial == 1)

				}//End if(touchedDial != hgDateDialer.getTouchedDial())

			}//End if(hgDateDialer.getBoxBehaviour() == 1)
			//Bottom of Block: Update UI when touched dial changes for out-of-the-box behaviour 1.

			dateStatus = Integer.toString(hgDateInfo.getYyMMdd()[2]) + "-" +
				Integer.toString(hgDateInfo.getYyMMdd()[1]) + "-" +
				Integer.toString(hgDateInfo.getYyMMdd()[0]);
			lblDateStatus.setText(dateStatus);

		}//End if(hgDateDialer.getBoxBehaviour() == 3 && hgDateDialer.getUseAngleSnap() == true)

	}

	@Override
	public void onPointerUp(HGDateDialer.HGDateInfo hgDateInfo) {/* Open End Will Probably Be Removed From Library */}

	@Override
	public void onUp(HGDateDialer.HGDateInfo hgDateInfo) {

		touchedDial = 0;

		//This block just sets up the time to time how long it takes to select a date and corresponds to code in the onDown
		if(hgDateDialer.getBoxBehaviour() != 1) {

			if(hgDateDialer.getWhatDial() == 1) {

				datePickDuration = System.currentTimeMillis() - datePickStartTime;
				Toast.makeText(getBaseContext(), "Yout took\n" + Long.toString(datePickDuration / 1000) + "\nSeconds to pick date", Toast.LENGTH_LONG).show();

			}

		}
		else if(hgDateDialer.getBoxBehaviour() == 1) {

			datePickDuration = System.currentTimeMillis() - datePickStartTime;
			Toast.makeText(getBaseContext(), "Yout took\n" + Long.toString(datePickDuration / 1000) + "\nSeconds to pick date", Toast.LENGTH_LONG).show();

		}//End if(hgDateDialer.getBoxBehaviour() != 1)

		//Programmatic behaviour to cause out-of-the-box behaviour 3 to snap to Jan 1st of every year when dialling year and first of every month when dialling month
		if(hgDateDialer.getBoxBehaviour() == 3 && hgDateDialer.getUseAngleSnap() == true) {

			if(hgDateDialer.getWhatDial() == 3) {

				dateStatus = "1" + "-" +
					Integer.toString(hgDateInfo.getYyMMdd()[1]) + "-" +
					Integer.toString(hgDateInfo.getYyMMdd()[0]);
				setInternalDate(dateStatus);
				lblDateStatus.setText(dateStatus);

			}
			else if(hgDateDialer.getWhatDial() == 2) {

				dateStatus = "1" + "-" +
					"1" + "-" +
					Integer.toString(hgDateInfo.getYyMMdd()[0]);
				setInternalDate(dateStatus);
				lblDateStatus.setText(dateStatus);

			}//End if(hgDateDialer.getWhatDial() == 3)

		}
		else {

			dateStatus = Integer.toString(hgDateInfo.getYyMMdd()[2]) + "-" +
				Integer.toString(hgDateInfo.getYyMMdd()[1]) + "-" +
				Integer.toString(hgDateInfo.getYyMMdd()[0]);
			lblDateStatus.setText(dateStatus);

		}//End if(hgDateDialer.getBoxBehaviour() == 3 && hgDateDialer.getUseAngleSnap() == true)

		//Update UI to show which dial is active.
		if(hgDateDialer.getBoxBehaviour() != 1) {

			if(hgDateDialer.getWhatDial() == 1) {

				btnYYYY.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));
				btnMM.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				btnDD.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));

			}
			else if(hgDateDialer.getWhatDial() == 2) {

				btnYYYY.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				btnMM.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));
				btnDD.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));

			}
			else if(hgDateDialer.getWhatDial() == 3) {

				btnYYYY.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				btnMM.setBackgroundColor(Color.parseColor(typedValue.coerceToString().toString()));
				btnDD.setBackgroundColor(Color.parseColor(typedValueActive.coerceToString().toString()));

			}//End if(hgDateDialer.getWhatDial() == 1)

		}//End if(hgDateDialer.getBoxBehaviour() != 1)

	}

}