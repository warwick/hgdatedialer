package com.WarwickWestonWright.HGDateDialerDemo;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.WarwickWestonWright.HGDateDialer.HGDateUtils;

import java.util.GregorianCalendar;

public class DatePickerDialog extends DialogFragment
	implements DatePicker.OnDateChangedListener,
	DialogInterface.OnShowListener {


	public interface IDatePickerDialog {

		void datePickerDialogCb(int year, int month, int date);

	}

	private IDatePickerDialog iDatePickerDialog;

	private View rootView;
	private Button btnSelectTime;
	private DatePicker oldDatePicker;

	private String dateString;
	private int year;
	private int month;
	private int date;

	private HGDateUtils hgDateUtils;

	public DatePickerDialog() {}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		hgDateUtils = new HGDateUtils(getContext());
		dateString = getArguments().getString("dateString");
		hgDateUtils.setInternalDate(dateString);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.date_picker_dialog_fragment, container, false);
		getDialog().setOnShowListener(this);

		btnSelectTime = (Button) rootView.findViewById(R.id.btnSelectTime);
		oldDatePicker = (DatePicker) rootView.findViewById(R.id.oldDatePicker);
		year = hgDateUtils.getGregorianCalendar().get(GregorianCalendar.YEAR);
		month = hgDateUtils.getGregorianCalendar().get(GregorianCalendar.MONTH);
		date = hgDateUtils.getGregorianCalendar().get(GregorianCalendar.DATE);

		btnSelectTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				iDatePickerDialog.datePickerDialogCb(oldDatePicker.getYear(), oldDatePicker.getMonth(), oldDatePicker.getDayOfMonth());

			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onShow(DialogInterface dialogInterface) {

		oldDatePicker.init(year, month, date, this);

	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if(context instanceof IDatePickerDialog) {

			iDatePickerDialog = (IDatePickerDialog) context;

		}
		else {

			throw new RuntimeException(context.toString() + " must implement IDatePickerDialog");

		}

	}


	@Override
	public void onDetach() {
		super.onDetach();

		iDatePickerDialog = null;

	}


	@Override
	public void onDateChanged(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

		this.year = year; this.month = monthOfYear; this.date = dayOfMonth;

	}

}