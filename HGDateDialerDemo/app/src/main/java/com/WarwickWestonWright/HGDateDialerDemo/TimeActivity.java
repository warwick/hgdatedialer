package com.WarwickWestonWright.HGDateDialerDemo;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.WarwickWestonWright.HGDateDialer.HGDateUtils;
import com.WarwickWestonWright.HGTimeDialer.HGMinuteDialer;
import com.WarwickWestonWright.HGTimeDialer.HGSecondDialer;
import com.WarwickWestonWright.HGTimeDialer.HGTimeDialer;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeActivity extends AppCompatActivity implements HGTimeDialer.IHGTimeDial {

	//Declare Layouts
	public HGTimeDialer hgTimeDialer;
	private HGMinuteDialer hgMinuteDialer;
	private HGSecondDialer hgSecondDialer;

	//Declare Custom Objects
	private HGDateUtils hgDateUtils;

	//Declare Widgets
	private TextView lblTimeStatus;
	private CheckBox chkRelativeTime;
	private CheckBox chkTimeSnapping;
	private CheckBox chkKeepTime;
	private CheckBox chkCumulative;
	private EditText txtMinuteSnap;
	private EditText txtSecondSnap;
	private EditText txtManualTime;
	private Button btnSetDialTime;

	//Declare Java Objects
	private SharedPreferences sp;
	private Pattern pattern;
	private Matcher matcher;
	private final KeepTimeHandler keepTimeHandler = new KeepTimeHandler(this);
	private Thread keepTime;
	private boolean doKeepTime;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.time_activity);
		getSupportActionBar().hide();
		sp = PreferenceManager.getDefaultSharedPreferences(this);
		hgDateUtils = new HGDateUtils(this);
		//Regular expression to check for valid time format.
		pattern = Pattern.compile("^\\d{1,2}:{1,}\\d{1,2}:{1,}\\d{1,2}$", Pattern.CASE_INSENSITIVE);

		/* Top of Block Usage Section */
		//Instantiate Layouts
		hgTimeDialer = (HGTimeDialer) findViewById(R.id.hgTimeDialer);
		hgMinuteDialer = (HGMinuteDialer) findViewById(R.id.hgMinuteDialer);
		hgSecondDialer = (HGSecondDialer) findViewById(R.id.hgMSecondDialer);

		//These 2 setters must me called before using any setters from the HGTimeDialer object.
		hgTimeDialer.setHgMinuteDialer(hgMinuteDialer);
		hgTimeDialer.setHgSecondDialer(hgSecondDialer);
		hgTimeDialer.registerCallback(this);
		/* Bottom of Block Usage Section */

		//Instantiate Widgets
		lblTimeStatus = (TextView) findViewById(R.id.lblTimeStatus);
		chkRelativeTime = (CheckBox) findViewById(R.id.chkRelativeTime);
		chkTimeSnapping = (CheckBox) findViewById(R.id.chkTimeSnapping);
		chkCumulative = (CheckBox) findViewById(R.id.chkCumulative);
		chkKeepTime = (CheckBox) findViewById(R.id.chkKeepTime);
		txtMinuteSnap = (EditText) findViewById(R.id.txtMinuteSnap);;
		txtSecondSnap = (EditText) findViewById(R.id.txtSecondSnap);;
		txtManualTime = (EditText) findViewById(R.id.txtManualTime);
		btnSetDialTime = (Button) findViewById(R.id.btnSetDialTime);

		//Populate form with persistent values
		chkRelativeTime.setChecked(sp.getBoolean("relativeTime", true));
		chkTimeSnapping.setChecked(sp.getBoolean("timeAngleSnap", true));
		chkCumulative.setChecked(sp.getBoolean("timeCumulativeRotate", true));
		chkKeepTime.setChecked(sp.getBoolean("keepTime", false));
		txtMinuteSnap.setText(sp.getString("minuteSnapTextVal", "5"));
		txtSecondSnap.setText(sp.getString("minuteSnapTextVal", "5"));

		doKeepTime = sp.getBoolean("keepTime", false);

		if(doKeepTime == true) {keepTime();}

		if(sp.getBoolean("timeAngleSnap", true) == true) {

			hgTimeDialer.setHourSnap(sp.getBoolean("timeAngleSnap", true));

			if(txtMinuteSnap.getText().toString().equals("") == false) {

				hgMinuteDialer.setMinuteSnap(Integer.parseInt(txtMinuteSnap.getText().toString()));

			}
			else if(txtMinuteSnap.getText().toString().equals("") == true) {

				hgMinuteDialer.setMinuteSnap(0);

			}//End if(txtMinuteSnap.getText().toString().equals("") == false)

			if(txtSecondSnap.getText().toString().equals("") == false) {

				hgSecondDialer.setSecondSnap(Integer.parseInt(txtSecondSnap.getText().toString()));

			}
			else if(txtSecondSnap.getText().toString().equals("") == true) {

				hgSecondDialer.setSecondSnap(0);

			}//End if(txtSecondSnap.getText().toString().equals("") == false)

		}
		else if(sp.getBoolean("timeAngleSnap", true) == false) {

			hgTimeDialer.setHourSnap(sp.getBoolean("timeAngleSnap", true));
			hgMinuteDialer.setMinuteSnap(0);
			hgSecondDialer.setSecondSnap(0);

		}//End else if(sp.getBoolean("timeAngleSnap", true) == false)

		hgTimeDialer.setCumulativeRotate(sp.getBoolean("timeCumulativeRotate", true));
		hgMinuteDialer.setCumulativeRotate(sp.getBoolean("timeCumulativeRotate", true));
		hgSecondDialer.setCumulativeRotate(sp.getBoolean("timeCumulativeRotate", true));
		hgTimeDialer.setRelativeTime(sp.getBoolean("relativeTime", true));

		//Set current time in the text control
		txtManualTime.setText(hgDateUtils.getTime());

		chkRelativeTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				sp.edit().putBoolean("relativeTime", chkRelativeTime.isChecked()).commit();
				hgTimeDialer.setRelativeTime(chkRelativeTime.isChecked());

			}
		});

		chkTimeSnapping.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				sp.edit().putBoolean("timeAngleSnap", chkTimeSnapping.isChecked()).commit();

				if(chkTimeSnapping.isChecked() == true && txtMinuteSnap.getText().toString().equals("") == false && txtSecondSnap.getText().toString().equals("") == false) {

					hgTimeDialer.setHourSnap(true);
					sp.edit().putInt("timeMinuteSnap", Integer.parseInt(txtMinuteSnap.getText().toString())).commit();
					sp.edit().putInt("timeSecondSnap", Integer.parseInt(txtSecondSnap.getText().toString())).commit();
					hgMinuteDialer.setMinuteSnap(sp.getInt("timeMinuteSnap", 5));
					hgSecondDialer.setSecondSnap(sp.getInt("timeSecondSnap", 5));

				}
				else if(chkTimeSnapping.isChecked() == false) {

					hgTimeDialer.setHourSnap(false);
					sp.edit().putInt("timeMinuteSnap", 0).commit();
					sp.edit().putInt("timeSecondSnap", 0).commit();
					hgMinuteDialer.setMinuteSnap(0);
					hgSecondDialer.setSecondSnap(0);

				}//End if(chkTimeSnapping.isChecked() == true && txtMinuteSnap.getText().toString().equals("") == false && txtSecondSnap.getText().toString().equals("") == false)

			}
		});

		chkCumulative.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				sp.edit().putBoolean("timeCumulativeRotate", chkCumulative.isChecked()).commit();
				hgTimeDialer.setCumulativeRotate(chkCumulative.isChecked());
				hgMinuteDialer.setCumulativeRotate(chkCumulative.isChecked());
				hgSecondDialer.setCumulativeRotate(chkCumulative.isChecked());

			}
		});

		chkKeepTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				sp.edit().putBoolean("keepTime", chkKeepTime.isChecked()).commit();
				doKeepTime = sp.getBoolean("keepTime", false);

				if(doKeepTime == true) {keepTime();;}

			}
		});

		btnSetDialTime.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(txtManualTime.getText().toString().equals("") == false) {

					matcher = pattern.matcher(txtManualTime.getText().toString());

					if(matcher.find() == true) {

						hgTimeDialer.setInternalTime(txtManualTime.getText().toString());
						lblTimeStatus.setText(hgTimeDialer.getInternalTime());

					}
					else if(matcher.find() == false) {

						Toast.makeText(getBaseContext(), getString(R.string.toast_invalid_time_error), Toast.LENGTH_LONG).show();

					}//End if(matcher.find() == true)

				}
				else if(txtManualTime.getText().toString().equals("") == true) {

					Toast.makeText(getBaseContext(), getString(R.string.toast_empty_time_error), Toast.LENGTH_LONG).show();

				}//End if(txtManualTime.getText().toString().equals("") == false)

			}
		});

	}//End protected void onCreate(Bundle savedInstanceState)


	private void setSnapping() {

		sp.edit().putString("minuteSnapTextVal", txtMinuteSnap.getText().toString()).commit();
		sp.edit().putString("minuteSnapTextVal", txtSecondSnap.getText().toString()).commit();

	}


	@Override
	protected void onStop() {
		super.onStop();

		setSnapping();

	}


	private void keepTime() {

		keepTime = new Thread(new Runnable() {
			@Override
			public void run() {

				keepTimeHandler.sendEmptyMessage(0);
				long interval = System.currentTimeMillis();

				while(doKeepTime == true) {

					if(System.currentTimeMillis() > interval + 1000) {

						interval = System.currentTimeMillis();
						keepTimeHandler.sendEmptyMessage(0);

					}//End if(System.currentTimeMillis() > interval + timeclick)

				}//End while(doKeepTime == true)

			}
		});

		keepTime.start();

	}//End private void keepTime()


	@Override
	public void onDown(HGTimeDialer.HGTimeInfo hgTimeInfo) {

		if(chkTimeSnapping.isChecked() == true) {

			hgTimeDialer.setHourSnap(true);
			hgMinuteDialer.setMinuteSnap(Integer.parseInt(txtMinuteSnap.getText().toString()));
			hgSecondDialer.setSecondSnap(Integer.parseInt(txtSecondSnap.getText().toString()));

		}

		lblTimeStatus.setText(hgTimeInfo.getInternalTime());

	}

	@Override
	public void onPointerDown(HGTimeDialer.HGTimeInfo hgTimeInfo) {}

	@Override
	public void onMove(HGTimeDialer.HGTimeInfo hgTimeInfo) {

		lblTimeStatus.setText(hgTimeInfo.getInternalTime());

	}

	@Override
	public void onPointerUp(HGTimeDialer.HGTimeInfo hgTimeInfo) {}

	@Override
	public void onUp(HGTimeDialer.HGTimeInfo hgTimeInfo) {

		lblTimeStatus.setText(hgTimeInfo.getInternalTime());
		/*Uncomment the following line to adjust only the hour
		hgTimeDialer.setWhatHand(HGTimeDialer.GESTURE_HOUR);
		*/

	}


	private static class KeepTimeHandler extends Handler {

		private final WeakReference<TimeActivity> timeActivityWeakReference;

		public KeepTimeHandler(TimeActivity timeActivity) {

			timeActivityWeakReference = new WeakReference<>(timeActivity);

		}

		@Override
		public void handleMessage(Message msg) {

			if(timeActivityWeakReference != null) {

				String currentTime = timeActivityWeakReference.get().hgDateUtils.getTime();
				timeActivityWeakReference.get().hgTimeDialer.setInternalTime(currentTime);
				timeActivityWeakReference.get().lblTimeStatus.setText(currentTime);

			}

		}//End public void handleMessage(Message msg)

	}//End private static class KeepTimeHandler extends Handler

}