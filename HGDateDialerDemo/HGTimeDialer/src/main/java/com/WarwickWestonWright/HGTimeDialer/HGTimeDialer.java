/*
License. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License
In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".
Copyright (c) 2016, Warwick Weston Wright All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.WarwickWestonWright.HGTimeDialer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class HGTimeDialer extends View implements
	HGMinuteDialer.IMinuteRelativeTime,
	HGSecondDialer.ISecondRelativeTime {

	/* Top of block field declarations */
	private OnTouchListener onTouchListener;
	private HGMinuteDialer.IMinuteRelativeTime iMinuteRelativeTime = this;
	private HGSecondDialer.ISecondRelativeTime iSecondRelativeTime = this;
	private static int contentWidth;
	private static int contentHeight;
	private static IHGTimeDial ihgTimeDial;
	private HGTimeInfo hgTimeInfo;
	private GetAngleWrapper getAngleWrapper;
	private HGMinuteDialer hgMinuteDialer;
	private HGSecondDialer hgSecondDialer;
	private static int touchPointerCount;
	private float storedGestureAngleBaseOne;
	private static int storedObjectCount;
	private float currentGestureAngleBaseOne;
	private float fullGestureAngleBaseOne;
	private static boolean cumulativeRotate;
	private static float onDownAngleObjectCumulativeBaseOne;
	private static float onUpAngleGestureBaseOne;
	private static float hourSnap;
	private static boolean hourSnapField;
	private static float angleSnapNextBaseOne;
	private static float angleSnapLastBaseOne;
	private static float angleSnapProximity;//Need to optimise the proximity setting as it is not needed in this library. This code is here from the refactored library.
	private static boolean hourHasSnapped;
	private static float touchOffsetX;
	private static float touchOffsetY;
	private static float touchXLocal;
	private static float touchYLocal;
	private static long quickTapTime;
	private static long gestureDownTime;
	private static boolean suppressInvalidate;
	private Drawable hourDrawable;
	private static float hourRelativeSize = 1f;
	static int whatHand;
	public static final int GESTURE_HOUR = 1;
	public static final int GESTURE_MINUTE = 2;
	public static final int GESTURE_SECOND = 3;
	private int hour;
	private int minute;
	private int second;
	private boolean relativeTime;
    /* Bottom of block field declarations */

	/* Top of block field declarations */
	public interface IHGTimeDial {

		void onDown(HGTimeInfo hgTimeInfo);
		void onPointerDown(HGTimeInfo hgTimeInfo);
		void onMove(HGTimeInfo hgTimeInfo);
		void onPointerUp(HGTimeInfo hgTimeInfo);
		void onUp(HGTimeInfo hgTimeInfo);

	}

	//Top of block main angle wrapper used by outer class
	final static class GetAngleWrapper {

		public GetAngleWrapper() {

			this.rotationGestureDirection = 0;
			this.rotationObjectDirection = 0;
			this.angleGestureBaseOne = 0f;
			this.angleObjectBaseOne = 0f;
			this.rotationGestureCount = 0;
			this.rotationObjectCount = 0;
			this.gestureTouchAngle = 0f;

		}//End public GetAngleWrapper()

		private Point viewCenterPoint;
		private int rotationGestureDirection;
		private int rotationObjectDirection;
		private int rotationGestureCount;
		private int rotationObjectCount;
		private float angleGestureBaseOne;
		private float angleObjectBaseOne;
		private float gestureTouchAngle;

		/* Accessors */
		public int getGestureRotationDirection() {return this.rotationGestureDirection;}
		public int getObjectRotationDirection() {return this.rotationObjectDirection;}
		public int getGestureRotationCount() {return this.rotationGestureCount;}
		public int getObjectRotationCount() {return this.rotationObjectCount;}
		public float getGestureAngleBaseOne() {return this.angleGestureBaseOne;}
		public float getObjectAngleBaseOne() {return this.angleObjectBaseOne;}
		public float getGestureTouchAngle() {return this.gestureTouchAngle;}

		/* Mutators */
		private void setGestureRotationDirection(final int rotationGestureDirection) {this.rotationGestureDirection = rotationGestureDirection;}
		private void setObjectRotationDirection(final int rotationObjectDirection) {this.rotationObjectDirection = rotationObjectDirection;}
		private void setGestureRotationCount(final int gestureRotationCount) {this.rotationGestureCount = gestureRotationCount;}
		private void setObjectRotationCount(final int objectRotationCount) {this.rotationObjectCount = objectRotationCount;}
		private void setGestureAngleBaseOne(final float angleBaseOne) {this.angleGestureBaseOne = angleBaseOne;}
		private void setObjectAngleBaseOne(final float precisionAngleBaseOne) {this.angleObjectBaseOne = precisionAngleBaseOne;}
		private void setGestureTouchAngle(final float gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}

	}//End final static class GetAngleWrapper


	public static class HGTimeInfo {

		private boolean quickTap;
		private int objectRotationDirection;
		private float objectAngleBaseOne;
		private float firstTouchX;
		private float firstTouchY;
		private boolean rotateSnapped;
		private int hour;
		private int minute;
		private int second;
		private String internalTime;

		public HGTimeInfo() {

			this.quickTap = false;
			this.objectRotationDirection = 0;
			this.objectAngleBaseOne = 0f;
			this.firstTouchX = 0f;
			this.firstTouchY = 0f;
			this.rotateSnapped = false;
			this.hour = 0;
			this.minute = 0;
			this.second = 0;
			internalTime = "00:00:00";

		}

		/* Accessors */
		public boolean getQuickTap() {return this.quickTap;}
		public int getObjectRotationDirection() {return this.objectRotationDirection;}
		public float getObjectAngleBaseOne() {return this.objectAngleBaseOne;}
		public float getFirstTouchX() {return this.firstTouchX;}
		public float getFirstTouchY() {return this.firstTouchY;}
		public boolean getRotateSnapped() {return this.rotateSnapped;}
		public int getHour() {return  this.hour;}
		public int getMinute() {return this.minute;}
		public int getSecond() {return this.second;}
		public String getInternalTime() {return this.internalTime;}

		/* Mutators */
		public void setQuickTap(boolean quickTap) {this.quickTap = quickTap;}
		public void setObjectRotationDirection(int objectRotationDirection) {this.objectRotationDirection = objectRotationDirection;}
		public void setObjectAngleBaseOne(float objectAngleBaseOne) {this.objectAngleBaseOne = objectAngleBaseOne;}
		public void setFirstTouchX(float firstTouchX) {this.firstTouchX = firstTouchX;}
		public void setFirstTouchY(float firstTouchY) {this.firstTouchY = firstTouchY;}
		public void setRotateSnapped(boolean rotateSnapped) {this.rotateSnapped = rotateSnapped;}
		public void setHour(int hour) {this.hour = hour;}
		public void setMinute(int minute) {this.minute = minute;}
		public void setSecond(int second) {this.second = second;}
		public void setInternalTime() {this.internalTime = Integer.toString(hour) + ":" + Integer.toString(minute) + ":" + Integer.toString(second);}

	}//End public static class HGTimeInfo
	//Bottom of block main angle wrapper used by outer class


	public HGTimeDialer(Context context) {
		super(context);

		setOnTouchListener(getOnTouchListerField());
		init(null, 0);
		setFields();

	}//End public HGTimeDialer(Context context)


	public HGTimeDialer(Context context, AttributeSet attrs) {
		super(context, attrs);

		setOnTouchListener(getOnTouchListerField());
		init(attrs, 0);
		setFields();

	}//End public HGTimeDialer(Context context, AttributeSet attrs)


	private void setFields() {

		this.contentWidth = 0;
		this.contentHeight = 0;
		this.ihgTimeDial = null;
		this.storedGestureAngleBaseOne = 0f;
		this.storedObjectCount = 0;
		this.currentGestureAngleBaseOne = 0f;
		this.fullGestureAngleBaseOne = 0f;
		this.touchPointerCount = 0;
		this.cumulativeRotate = true;
		this.onDownAngleObjectCumulativeBaseOne = 0f;
		this.onUpAngleGestureBaseOne = 0f;
		this.hourSnap = 0f;
		this.hourSnapField = true;
		this.angleSnapProximity = 0f;
		this.hourHasSnapped = false;
		this.touchOffsetX = 0;
		this.touchOffsetY = 0;
		this.touchXLocal = 0f;
		this.touchYLocal = 0f;
		this.quickTapTime = 0;
		this.gestureDownTime = 100;
		this.suppressInvalidate = false;
		this.whatHand = GESTURE_HOUR;
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
		this.relativeTime = true;
		this.angleSnapNextBaseOne = 0f;
		this.angleSnapLastBaseOne = this.angleSnapNextBaseOne;
		this.getAngleWrapper = new GetAngleWrapper();
		this.hgTimeInfo = new HGTimeInfo();

	}//End private void setFields()


	public void registerCallback(IHGTimeDial ihgTimeDial) {

		HGTimeDialer.ihgTimeDial = ihgTimeDial;

	}


	private void init(AttributeSet attrs, int defStyle) {

		final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HGTimeDialer, defStyle, 0);

		if(a.hasValue(R.styleable.HGTimeDialer_hourDrawable)) {

			hourDrawable = a.getDrawable(R.styleable.HGTimeDialer_hourDrawable);
			hourDrawable.setCallback(this);

		}

		a.recycle();

	}//End private void init(AttributeSet attrs, int defStyle)


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if(getAngleWrapper.viewCenterPoint != null) {

			if(hourSnap == 0) {

				canvas.rotate((getAngleWrapper.getObjectAngleBaseOne() * 360), getAngleWrapper.viewCenterPoint.x, getAngleWrapper.viewCenterPoint.y);

			}
			else /* if(hourSnap != 0) */ {

				canvas.rotate((angleSnapNextBaseOne * 360), getAngleWrapper.viewCenterPoint.x, getAngleWrapper.viewCenterPoint.y);

			}//End if(rapidDial != 0)

			hourDrawable.draw(canvas);

		}
		else /* if(getAngleWrapper.viewCenterPoint == null) */ {

			contentWidth = getWidth();
			contentHeight = getHeight();
			getAngleWrapper.viewCenterPoint = new Point(contentWidth / 2, contentHeight / 2);
			hourDrawable.setBounds(
				getAngleWrapper.viewCenterPoint.x - (int) ((float) contentWidth * (hourRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y - (int) ((float) contentHeight * (hourRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.x + (int) ((float) contentWidth * (hourRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y + (int) ((float) contentHeight * (hourRelativeSize / 2f)));

			if(hourDrawable != null) {

				hourDrawable.draw(canvas);

			}

		}//End if(getAngleWrapper.viewCenterPoint != null)

	}//End protected void onDraw(Canvas canvas)


	public Drawable getHourDrawable() {return hourDrawable;}
	public void setHourDrawable(Drawable hourDrawable) {this.hourDrawable = hourDrawable;}


	/* Top of block touch methods */
	private void setDownTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					touchXLocal = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					touchYLocal = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgTimeInfo.setFirstTouchX(touchXLocal);
					hgTimeInfo.setFirstTouchY(touchYLocal);

				}

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setDownTouch(final MotionEvent event)


	private void setMoveTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					touchXLocal = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					touchYLocal = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgTimeInfo.setFirstTouchX(touchXLocal);
					hgTimeInfo.setFirstTouchY(touchYLocal);

				}

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setMoveTouch(final MotionEvent event)


	private void setUpTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					touchXLocal = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					touchYLocal = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgTimeInfo.setFirstTouchX(touchXLocal);
					hgTimeInfo.setFirstTouchY(touchYLocal);

				}

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


	/* Top of block rotate functions */
	private HGTimeInfo doDownDial() {

		//Prepare touches for single or dual touch
		if(touchPointerCount > 1) {

			return hgTimeInfo;

		}

		getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));

		if(cumulativeRotate == true) {

			onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;

		}
		else if(cumulativeRotate == false) {

			if(!(getAngleWrapper.getGestureTouchAngle() < 0.5f)) {

				getAngleWrapper.setGestureTouchAngle(-(1f - getAngleWrapper.getGestureTouchAngle()));

			}

			onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;
			fullGestureAngleBaseOne = getAngleWrapper.getGestureTouchAngle();

		}//End if(cumulativeRotate == true)

		setReturnType();

		return hgTimeInfo;

	}//End private HGTimeInfo doDownDial()


	private HGTimeInfo doMoveDial() {

		//Prepare touches for single or dual touch
		if(touchPointerCount > 1) {

			return hgTimeInfo;

		}

		getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));
		setReturnType();

		return hgTimeInfo;

	}//End private HGTimeInfo doMoveDial()


	private HGTimeInfo doUpDial() {

		//Prepare touches for single or dual touch
		if(touchPointerCount > 1) {

			return hgTimeInfo;

		}

		if(hgTimeInfo.getQuickTap() == false) {

			getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));
			onUpAngleGestureBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle())) % 1;

			setReturnType();

			if(whatHand == GESTURE_HOUR) {

				whatHand = GESTURE_MINUTE;

			}

			if(fullGestureAngleBaseOne < 0f) {

				if(hourSnapField == true) {

					hour = (int) (((angleSnapNextBaseOne) / (1f / 12f)) + 24);

				}
				else if(hourSnapField == false) {

					hour = (int) (((fullGestureAngleBaseOne) / (1f / 12f)) + 24);

				}//End if(hourSnapField == true)

			}
			else if(fullGestureAngleBaseOne >= 0f) {

				if(hourSnapField == true) {

					hour = (int) (angleSnapNextBaseOne / (1f / 12f));

				}
				else if(hourSnapField == false) {

					hour = (int) (fullGestureAngleBaseOne / (1f / 12f));

				}//End if(hourSnapField == true)

			}//End if(fullGestureAngleBaseOne < 0f)

		}//End if(hgTimeInfo.getQuickTap() == false)

		return hgTimeInfo;

	}//End private HGTimeInfo doUpDial()
	/* Bottom of block rotate functions */


	private void setReturnType() {

		setRotationAngleGestureAndReturnDirection();
		hgTimeInfo.setObjectRotationDirection(getAngleWrapper.getObjectRotationDirection());
		hgTimeInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());

		if(hourSnap == 0f) {

			if(whatHand == GESTURE_HOUR) {

				if(fullGestureAngleBaseOne < 0f) {

					hour = (int) ((2f + fullGestureAngleBaseOne) / (1f / 12f));

				}
				else /* if(fullGestureAngleBaseOne >= 0f) */ {

					hour = (int) (fullGestureAngleBaseOne / (1f / 12f));

				}//End if(fullGestureAngleBaseOne < 0f)

				hgTimeInfo.setHour(hour);

			}//End if(whatHand == GESTURE_HOUR)

		}
		else /* if(hourSnap != 0f) */ {

			if(whatHand == GESTURE_HOUR) {

				this.angleSnapLastBaseOne = this.angleSnapNextBaseOne;
				checkNextHourSnap();

				if(this.angleSnapLastBaseOne != this.angleSnapNextBaseOne) {

					if(fullGestureAngleBaseOne < 0f) {

						hour = (Math.round((2f + fullGestureAngleBaseOne) / (1f / 12f)) % 24);

					}
					else /* if(fullGestureAngleBaseOne >= 0f) */ {

						hour = Math.round(fullGestureAngleBaseOne / (1f / 12f));

					}//End if(fullGestureAngleBaseOne < 0f)

					hgTimeInfo.setHour(hour);

				}//End if(this.angleSnapLastBaseOne != this.angleSnapNextBaseOne)

			}//End if(whatHand == GESTURE_HOUR)

		}//End if(hourSnap == 0f)

		hgTimeInfo.setInternalTime();
		calculateObjectAngleBaseOne();

		if(suppressInvalidate == false) {

			invalidate();

		}

	}//End private void setReturnType()
    /* Bottom of block rotate functions */


	/* Top of block main functions */
	private int setRotationAngleGestureAndReturnDirection() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final float angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne);

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullGestureAngleBaseOne -= (angleDifference + 1f) % 1f;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullGestureAngleBaseOne += -angleDifference % 1f;

			}

			getAngleWrapper.setObjectRotationDirection(returnValue[0]);

		}
		//Detect count boundary
		else /* if((Math.abs(angleDifference) > 0.75f)) */ {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullGestureAngleBaseOne += 1f - angleDifference;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullGestureAngleBaseOne -= (angleDifference + 1f) % 1f;

			}

			getAngleWrapper.setObjectRotationDirection(returnValue[0]);

		}//End if(!(Math.abs(storedGestureAngleBaseOne - currentGestureAngleBaseOne) > 0.75f))

		if(fullGestureAngleBaseOne < -1f) {

			fullGestureAngleBaseOne = 1f;

		}
		else if(fullGestureAngleBaseOne > 1f) {

			fullGestureAngleBaseOne = -1f;

		}//End if(fullGestureAngleBaseOne < 1f)

		getAngleWrapper.setGestureRotationCount((int) fullGestureAngleBaseOne);
		getAngleWrapper.setGestureAngleBaseOne(fullGestureAngleBaseOne % 1);
		storedObjectCount = (int) (fullGestureAngleBaseOne);
		getAngleWrapper.setObjectRotationCount(storedObjectCount);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

		return returnValue[0];

	}//End private int setRotationAngleGestureAndReturnDirection()


	private void calculateObjectAngleBaseOne() {getAngleWrapper.setObjectAngleBaseOne(((getAngleWrapper.getGestureRotationCount() + getAngleWrapper.getGestureAngleBaseOne())) % 1);}


	private void checkNextHourSnap() {

		final float tempAngleSnap = Math.round(getAngleWrapper.getObjectAngleBaseOne() / hourSnap);
		angleSnapNextBaseOne = tempAngleSnap * hourSnap;

		if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity) {

			hourHasSnapped = true;
			hgTimeInfo.setRotateSnapped(true);

		}
		else {

			angleSnapNextBaseOne = getAngleWrapper.getObjectAngleBaseOne();
			hourHasSnapped = false;
			hgTimeInfo.setRotateSnapped(false);

		}//End if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity)

	}//End private void checkNextHourSnap()
    /* Bottom of block main functions */


	/* Top of block Accessors */
	public GetAngleWrapper getAngleWrapper() {return this.getAngleWrapper;}
	public long getQuickTapTime() {return this.quickTapTime;}
	public boolean getCumulativeRotate() {return this.cumulativeRotate;}
	public boolean getHourSnap() {return this.hourSnapField;}
	public OnTouchListener retrieveLocalOnTouchListener() {return onTouchListener;}
	public float getTouchOffsetX() {return this.touchOffsetX;}
	public float getTouchOffsetY() {return this.touchOffsetY;}
	public boolean getHourHasAngleSnapped() {return hourHasSnapped;}
	public boolean getSuppressInvalidate() {return this.suppressInvalidate;}
	public int getWhatHand() {return this.whatHand;}
	public boolean getRelativeTime() {return this.relativeTime;}
    /* Bottom of block Accessors */


	/* Top of block Mutators */
	public void setQuickTapTime(long quickTapTime) {this.quickTapTime = quickTapTime;}
	public void setCumulativeRotate(final boolean cumulativeRotate) {this.cumulativeRotate = cumulativeRotate;}
	public void setSuppressInvalidate(final boolean suppressInvalidate) {HGTimeDialer.suppressInvalidate = suppressInvalidate;}


	public void setHgMinuteDialer(HGMinuteDialer hgMinuteDialer) {

		this.hgMinuteDialer = hgMinuteDialer;
		this.hgMinuteDialer.setHgTimeInfo(hgTimeInfo);

	}


	public void setHgSecondDialer(HGSecondDialer hgSecondDialer) {

		this.hgSecondDialer = hgSecondDialer;
		this.hgSecondDialer.setHgTimeInfo(hgTimeInfo);

	}


	public void setWhatHand(int whatHand) {this.whatHand = whatHand;}
	public String getInternalTime() {return Integer.toString(hour) + ":" + Integer.toString(minute) + ":" + Integer.toString(second);}


	public void setHourSnap(boolean hourSnap) {

		this.hourSnapField = hourSnap;

		if(hourSnap == false) {

			this.hourSnap = 0f;

		}
		else if(hourSnap == true) {

			this.hourSnap = (1f /12f) % 1;
			this.angleSnapProximity = (angleSnapProximity) % 1;
			angleSnapProximity = (1f /24f) % 1;

		}//End if(hourSnap == false)

	}//End public void setHourSnap(boolean hourSnap)


	public void setTouchOffset(float touchOffsetX, float touchOffsetY) {

		HGTimeDialer.touchOffsetX = touchOffsetX;
		HGTimeDialer.touchOffsetY = touchOffsetY;

	}//End public void setTouchOffset(float touchOffsetX, float touchOffsetY)

	public void setInternalTime(String time) {

		final boolean storedHS = getHourSnap();
		final int storedMS = hgMinuteDialer.getMinuteSnap();
		final int storedSS = hgSecondDialer.getSecondSnap();

		String[] timeParts = time.split(":", 3);

		if(timeParts.length != 3) {

			this.hour = 0;
			this.minute = 0;
			this.second = 0;

		}
		else if(timeParts.length == 3) {

			try {

				setHourSnap(false);
				hgMinuteDialer.setMinuteSnap(0);
				hgSecondDialer.setSecondSnap(0);
				hour = Integer.parseInt(timeParts[0]);
				hgTimeInfo.setHour(hour);
				minute = Integer.parseInt(timeParts[1]);
				hgTimeInfo.setMinute(minute);
				second = Integer.parseInt(timeParts[2]);
				hgTimeInfo.setSecond(second);

				if(hour > 12) {

					fullGestureAngleBaseOne = (1f / 12f) * ((float) (hour - 24));

				}
				else if(hour <= 12) {

					fullGestureAngleBaseOne = ((1f / 12f) * (float) hour);

				}//End if(hour > 12)

				if(relativeTime == true) {

					if(hour > 12) {

						fullGestureAngleBaseOne += (1f / 60f / 12f) * minute;

					}
					else if(hour <= 12) {

						fullGestureAngleBaseOne += (1f / 60f / 12f) * minute;

					}//End if(hour > 12)

				}//End if(relativeTime == true)

				getAngleWrapper.setObjectAngleBaseOne(fullGestureAngleBaseOne);
				checkNextHourSnap();
				invalidate();
				hgMinuteDialer.fullGestureAngleBaseOne = ((1f / 60f) * (float) minute);
				hgMinuteDialer.getAngleWrapper.setObjectAngleBaseOne(hgMinuteDialer.fullGestureAngleBaseOne);
				hgMinuteDialer.checkNextMinuteSnap();
				hgMinuteDialer.invalidate();
				hgSecondDialer.fullGestureAngleBaseOne = ((1f / 60f) * (float) second);
				hgSecondDialer.getAngleWrapper.setObjectAngleBaseOne(hgSecondDialer.fullGestureAngleBaseOne);
				hgSecondDialer.checkNextSecondSnap();
				hgSecondDialer.invalidate();

			}
			catch(NumberFormatException e) {

				this.hour = 0;
				this.minute = 0;
				this.second = 0;
				setHourSnap(storedHS);
				hgMinuteDialer.setMinuteSnap(storedMS);
				hgSecondDialer.setSecondSnap(storedSS);

			}

		}//End if(timeParts.length != 3)

		setHourSnap(storedHS);
		hgMinuteDialer.setMinuteSnap(storedMS);
		hgSecondDialer.setSecondSnap(storedSS);

	}//End public void setInternalTime(String time)


	public void setRelativeTime(boolean relativeTime) {

		this.relativeTime = relativeTime;
		hgMinuteDialer.setRelativeTime(relativeTime);
		hgSecondDialer.setRelativeTime(relativeTime);

		if(relativeTime == true) {

			hgMinuteDialer.setIIMinuteRelativeTime(iMinuteRelativeTime);
			hgSecondDialer.setISecondRelativeTime(iSecondRelativeTime);

		}
		else if(relativeTime == false) {

			hgMinuteDialer.setIIMinuteRelativeTime(null);
			hgSecondDialer.setISecondRelativeTime(null);

		}//End if(relativeTime == true)

	}//End public void setRelativeTime(boolean relativeTime)
    /* Bottom of block Mutators */


	public void performQuickTap() {

		hgTimeInfo.setQuickTap(true);

		post(new Runnable() {
			@Override
			public void run() {

				ihgTimeDial.onUp(doUpDial());
				hgTimeInfo.setQuickTap(false);

			}
		});

	}//End public void performQuickTap()


	/* Top of block geometry functions */
	public float getAngleFromPoint(final Point centerPoint, final Point touchPoint) {

		float returnVal = 0;

		//+0 - 0.5
		if(touchPoint.x > centerPoint.x) {

			returnVal = (float) (Math.atan2((touchPoint.x - centerPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI);

		}
		//+0.5
		else if(touchPoint.x < centerPoint.x) {

			returnVal = (float)  (1 - (Math.atan2((centerPoint.x - touchPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI));

		}//End if(touchPoint.x > centerPoint.x)

		return returnVal;

	}//End public float getAngleFromPoint(final Point centerPoint, final Point touchPoint)


	public Point getPointFromAngle(final float angle) {return getPointFromAngle(angle, getAngleWrapper.viewCenterPoint.x);}


	public Point getPointFromAngle(final float angle, final float radius) {

		final Point coords = new Point();
		coords.x = (int) (radius - (radius * Math.cos((angle - 0.75f) * 2 * Math.PI)));
		coords.y = (int) (radius - (radius * Math.sin((angle - 0.75f) * 2 * Math.PI)));

		return coords;

	}//End public Point getPointFromAngle(final float angle, final float radius)
    /* Bottom of block geometry functions */


	@Override
	public boolean onTouchEvent(MotionEvent event) {

		if(whatHand == GESTURE_HOUR) {

			sendTouchEvent(this, event);

		}

		return true;

	}


	private OnTouchListener getOnTouchListerField() {

		onTouchListener = new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				hgTouchEvent(v, event);

				return true;

			}

		};

		return onTouchListener;

	}//End private OnTouchListener getOnTouchListerField()


	public void sendTouchEvent(View v, MotionEvent event) {

		hgTouchEvent(v, event);

	}


	@Override
	public void setMinuteAngle(float minuteAngle) {

		if(fullGestureAngleBaseOne < 0f) {

			fullGestureAngleBaseOne = (((float) (hour - 24)) * (1f / 12f)) + minuteAngle;

		}
		else if(fullGestureAngleBaseOne >= 0f) {

			fullGestureAngleBaseOne = (((float) hour) * (1f / 12f)) + minuteAngle;

		}//End if(fullGestureAngleBaseOne < 0f)

		if(relativeTime == true && hourSnapField == true) {

			if(hgMinuteDialer.minute == 0) {

				if(fullGestureAngleBaseOne < 0f) {

					angleSnapNextBaseOne = (((float) (hour - 24)) * (1f / 12f));

				}
				else if(fullGestureAngleBaseOne >= 0f) {

					angleSnapNextBaseOne = (((float) hour) * (1f / 12f));

				}//End if(fullGestureAngleBaseOne < 0f)

			}
			else if(hgMinuteDialer.minute != 0) {

				angleSnapNextBaseOne = fullGestureAngleBaseOne;

			}//End if(hgMinuteDialer.minute == 0)

		}//End if(relativeTime == true && hourSnapField == true)

		setReturnType();
		invalidate();

	}//End public void setMinuteAngle(float minuteAngle)


	@Override
	public void setSecondAngle(float secondAngle) {

		if(relativeTime == true) {

			if(hgMinuteDialer.fullGestureAngleBaseOne < 0f) {

				hgMinuteDialer.fullGestureAngleBaseOne = hgMinuteDialer.getStoredRelativeAngle() + secondAngle;

			}
			else if(hgMinuteDialer.fullGestureAngleBaseOne >= 0f) {

				hgMinuteDialer.fullGestureAngleBaseOne = hgMinuteDialer.getStoredRelativeAngle() + secondAngle;

			}//End if(hgMinuteDialer.fullGestureAngleBaseOne < 0f)

			if(hourSnapField == true) {

				hgMinuteDialer.checkNextMinuteSnap();
				hgMinuteDialer.angleSnapNextBaseOne = (hgMinuteDialer.minute * (1f / 60f)) + secondAngle;
				hgMinuteDialer.fullGestureAngleBaseOne = HGMinuteDialer.angleSnapNextBaseOne;

			}
			else if(hourSnapField == false) {

				hgMinuteDialer.setReturnType();

			}//End if(hourSnapField == true)

		}//End if(relativeTime == true)

		hgMinuteDialer.invalidate();

	}//End public void setSecondAngle(float secondAngle)


	private void hgTouchEvent(View v, MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		//Top of block used for quick tap
		if(event.getAction() == MotionEvent.ACTION_DOWN) {

			hgTimeInfo.setQuickTap(false);
			gestureDownTime = System.currentTimeMillis();

		}
		else if(event.getAction() == MotionEvent.ACTION_UP) {

			//Used for quick tap
			if(System.currentTimeMillis() < gestureDownTime + quickTapTime) {

				hgTimeInfo.setQuickTap(true);
				ihgTimeDial.onUp(doUpDial());
				return;

			}
			else {

				hgTimeInfo.setQuickTap(false);

			}//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

		}//End if(event.getAction() == MotionEvent.ACTION_DOWN)
		//Bottom of block used for quick tap

		if(whatHand == GESTURE_HOUR) {

			switch(action) {

				case MotionEvent.ACTION_DOWN: {

					setDownTouch(event);
					ihgTimeDial.onDown(doDownDial());

					break;

				}
				case MotionEvent.ACTION_POINTER_DOWN: {

					setDownTouch(event);
					ihgTimeDial.onDown(doDownDial());

					break;

				}
				case MotionEvent.ACTION_MOVE: {

					setMoveTouch(event);
					ihgTimeDial.onMove(doMoveDial());

					break;

				}
				case MotionEvent.ACTION_POINTER_UP: {

					setUpTouch(event);
					ihgTimeDial.onUp(doUpDial());

					break;

				}
				case MotionEvent.ACTION_UP: {

					setUpTouch(event);
					ihgTimeDial.onUp(doUpDial());

					break;

				}
				default:

					break;

			}//End switch(action)

		}
		else if(whatHand == GESTURE_MINUTE) {

			switch(action) {

				case MotionEvent.ACTION_DOWN: {

					hgMinuteDialer.setDownTouch(event);
					ihgTimeDial.onDown(hgMinuteDialer.doDownDial());

					break;

				}
				case MotionEvent.ACTION_POINTER_DOWN: {

					hgMinuteDialer.setDownTouch(event);
					ihgTimeDial.onDown(hgMinuteDialer.doDownDial());

					break;

				}
				case MotionEvent.ACTION_MOVE: {

					hgMinuteDialer.setMoveTouch(event);
					ihgTimeDial.onMove(hgMinuteDialer.doMoveDial());

					break;

				}
				case MotionEvent.ACTION_POINTER_UP: {

					hgMinuteDialer.setUpTouch(event);
					ihgTimeDial.onUp(hgMinuteDialer.doUpDial());

					break;

				}
				case MotionEvent.ACTION_UP: {

					hgMinuteDialer.setUpTouch(event);
					ihgTimeDial.onUp(hgMinuteDialer.doUpDial());

					break;

				}
				default:

					break;

			}//End switch(action)

		}
		else if(whatHand == GESTURE_SECOND) {

			switch(action) {

				case MotionEvent.ACTION_DOWN: {

					hgSecondDialer.setDownTouch(event);
					ihgTimeDial.onDown(hgSecondDialer.doDownDial());

					break;

				}
				case MotionEvent.ACTION_POINTER_DOWN: {

					hgSecondDialer.setDownTouch(event);
					ihgTimeDial.onDown(hgSecondDialer.doDownDial());

					break;

				}
				case MotionEvent.ACTION_MOVE: {

					hgSecondDialer.setMoveTouch(event);
					ihgTimeDial.onMove(hgSecondDialer.doMoveDial());

					break;

				}
				case MotionEvent.ACTION_POINTER_UP: {

					hgSecondDialer.setUpTouch(event);
					ihgTimeDial.onUp(hgSecondDialer.doUpDial());

					break;

				}
				case MotionEvent.ACTION_UP: {

					hgSecondDialer.setUpTouch(event);
					ihgTimeDial.onUp(hgSecondDialer.doUpDial());

					break;

				}
				default:

					break;

			}//End switch(action)

		}//End if(whatHand == GESTURE_HOUR)

	}//End private void hgTouchEvent(View v, MotionEvent event)

}