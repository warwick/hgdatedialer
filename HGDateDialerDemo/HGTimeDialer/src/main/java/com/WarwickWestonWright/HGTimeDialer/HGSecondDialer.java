package com.WarwickWestonWright.HGTimeDialer;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import static com.WarwickWestonWright.HGTimeDialer.HGTimeDialer.GESTURE_HOUR;
import static com.WarwickWestonWright.HGTimeDialer.HGTimeDialer.GESTURE_SECOND;
import static com.WarwickWestonWright.HGTimeDialer.HGTimeDialer.whatHand;

public class HGSecondDialer extends View {

	/* Top of block field declarations */
	private ISecondRelativeTime iSecondRelativeTime;
	private static int contentWidth;
	private static int contentHeight;
	private HGTimeDialer.HGTimeInfo hgTimeInfo;
	GetAngleWrapper getAngleWrapper;
	private static int touchPointerCount;
	private float storedGestureAngleBaseOne;
	private static int storedObjectCount;
	private float currentGestureAngleBaseOne;
	float fullGestureAngleBaseOne;
	private static boolean cumulativeRotate;
	private static float onDownAngleObjectCumulativeBaseOne;
	private static float onUpAngleGestureBaseOne;
	private static float secondSnap;
	private static int secondSnapField;
	private static float angleSnapNextBaseOne;
	private static float angleSnapLastBaseOne;
	private static float angleSnapProximity;//Need to optimise the proximity setting as it is not needed in this library. This code is here from the refactored library.
	private static boolean secondHasSnapped;
	private static float touchOffsetX;
	private static float touchOffsetY;
	private static float touchXLocal;
	private static float touchYLocal;
	private static boolean suppressInvalidate;
	private Drawable secondDrawable;
	private static float secondRelativeSize = 0.8f;
	private boolean relativeTime;
	int second;
    /* Bottom of block field declarations */


	public interface ISecondRelativeTime {

		void setSecondAngle(float secondAngle);

	}


	//Top of block main angle wrapper used by outer class
	final public class GetAngleWrapper {

		public GetAngleWrapper() {

			this.rotationGestureDirection = 0;
			this.rotationObjectDirection = 0;
			this.angleGestureBaseOne = 0f;
			this.angleObjectBaseOne = 0f;
			this.rotationGestureCount = 0;
			this.rotationObjectCount = 0;
			this.gestureTouchAngle = 0f;

		}//End public GetAngleWrapper()

		private Point viewCenterPoint;
		private int rotationGestureDirection;
		private int rotationObjectDirection;
		private int rotationGestureCount;
		private int rotationObjectCount;
		private float angleGestureBaseOne;
		private float angleObjectBaseOne;
		private float gestureTouchAngle;

		/* Accessors */
		public int getGestureRotationDirection() {return this.rotationGestureDirection;}
		public int getObjectRotationDirection() {return this.rotationObjectDirection;}
		public int getGestureRotationCount() {return this.rotationGestureCount;}
		public int getObjectRotationCount() {return this.rotationObjectCount;}
		public float getGestureAngleBaseOne() {return this.angleGestureBaseOne;}
		public float getObjectAngleBaseOne() {return this.angleObjectBaseOne;}
		public float getGestureTouchAngle() {return this.gestureTouchAngle;}

		/* Mutators */
		private void setGestureRotationDirection(final int rotationGestureDirection) {this.rotationGestureDirection = rotationGestureDirection;}
		private void setObjectRotationDirection(final int rotationObjectDirection) {this.rotationObjectDirection = rotationObjectDirection;}
		private void setGestureRotationCount(final int gestureRotationCount) {this.rotationGestureCount = gestureRotationCount;}
		private void setObjectRotationCount(final int objectRotationCount) {this.rotationObjectCount = objectRotationCount;}
		private void setGestureAngleBaseOne(final float angleBaseOne) {this.angleGestureBaseOne = angleBaseOne;}
		void setObjectAngleBaseOne(final float precisionAngleBaseOne) {this.angleObjectBaseOne = precisionAngleBaseOne;}
		private void setGestureTouchAngle(final float gestureTouchAngle) {this.gestureTouchAngle = gestureTouchAngle;}

	}//End final public class GetAngleWrapper
	//Bottom final public class GetAngleWrapper


	public HGSecondDialer(Context context) {
		super(context);

		init(null, 0);
		setFields();

	}//End public HGSecondDialer(Context context)


	public HGSecondDialer(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(attrs, 0);
		setFields();

	}//End public HGSecondDialer(Context context, AttributeSet attrs)


	private void setFields() {

		this.contentWidth = 0;
		this.contentHeight = 0;
		this.storedGestureAngleBaseOne = 0f;
		this.storedObjectCount = 0;
		this.currentGestureAngleBaseOne = 0f;
		this.fullGestureAngleBaseOne = 0f;
		this.touchPointerCount = 0;
		this.cumulativeRotate = true;
		this.onDownAngleObjectCumulativeBaseOne = 0f;
		this.onUpAngleGestureBaseOne = 0f;
		this.secondSnap = 0f;
		this.secondSnapField = 0;
		this.angleSnapProximity = 0f;
		this.secondHasSnapped = false;
		this.touchOffsetX = 0;
		this.touchOffsetY = 0;
		this.touchXLocal = 0f;
		this.touchYLocal = 0f;
		this.suppressInvalidate = false;
		this.angleSnapNextBaseOne = 0f;
		this.angleSnapLastBaseOne = this.angleSnapNextBaseOne;
		this.relativeTime = true;
		this.second = 0;
		this.getAngleWrapper = new GetAngleWrapper();
		this.hgTimeInfo = new HGTimeDialer.HGTimeInfo();

	}//End private void setFields()


	private void init(AttributeSet attrs, int defStyle) {

		final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HGSecondDialer, defStyle, 0);

		if(a.hasValue(R.styleable.HGSecondDialer_secondDrawable)) {

			secondDrawable = a.getDrawable(R.styleable.HGSecondDialer_secondDrawable);
			secondDrawable.setCallback(this);

		}

		a.recycle();

	}//End private void init(AttributeSet attrs, int defStyle)


	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if(getAngleWrapper.viewCenterPoint != null) {

			if(secondSnap == 0) {

				canvas.rotate((getAngleWrapper.getObjectAngleBaseOne() * 360), getAngleWrapper.viewCenterPoint.x, getAngleWrapper.viewCenterPoint.y);

			}
			else /* if(secondSnap != 0) */ {

				canvas.rotate((angleSnapNextBaseOne * 360), getAngleWrapper.viewCenterPoint.x, getAngleWrapper.viewCenterPoint.y);

			}//End if(rapidDial != 0)

			secondDrawable.draw(canvas);

		}
		else /* if(getAngleWrapper.viewCenterPoint == null) */ {

			contentWidth = getWidth();
			contentHeight = getHeight();
			getAngleWrapper.viewCenterPoint = new Point(contentWidth / 2, contentHeight / 2);
			secondDrawable.setBounds(
				getAngleWrapper.viewCenterPoint.x - (int) ((float) contentWidth * (secondRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y - (int) ((float) contentHeight * (secondRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.x + (int) ((float) contentWidth * (secondRelativeSize / 2f)),
				getAngleWrapper.viewCenterPoint.y + (int) ((float) contentHeight * (secondRelativeSize / 2f)));

			if(secondDrawable != null) {

				secondDrawable.draw(canvas);

			}

		}//End if(getAngleWrapper.viewCenterPoint != null)

	}//End protected void onDraw(Canvas canvas)


	public Drawable getSecondDrawable() {return secondDrawable;}
	public void setSecondDrawable(Drawable secondDrawable) {this.secondDrawable = secondDrawable;}


	/* Top of block touch methods */
	void setDownTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					touchXLocal = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					touchYLocal = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgTimeInfo.setFirstTouchX(touchXLocal);
					hgTimeInfo.setFirstTouchY(touchYLocal);

				}

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End void setDownTouch(final MotionEvent event)


	void setMoveTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					touchXLocal = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					touchYLocal = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgTimeInfo.setFirstTouchX(touchXLocal);
					hgTimeInfo.setFirstTouchY(touchYLocal);

				}

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End void setMoveTouch(final MotionEvent event)


	void setUpTouch(final MotionEvent event) {

		try {

			try {

				touchPointerCount = event.getPointerCount();

				if(touchPointerCount == 1) {

					touchXLocal = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
					touchYLocal = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
					hgTimeInfo.setFirstTouchX(touchXLocal);
					hgTimeInfo.setFirstTouchY(touchYLocal);

				}

			}
			catch(IndexOutOfBoundsException e) {

				return;

			}

		}
		catch(IllegalArgumentException e) {

			return;

		}

	}//End void setUpTouch(final MotionEvent event)
	/* Bottom of block touch methods */


	/* Top of block rotate functions */
	HGTimeDialer.HGTimeInfo doDownDial() {

		//Prepare touches for single or dual touch
		if(touchPointerCount > 1) {

			return hgTimeInfo;

		}

		getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));

		if(cumulativeRotate == true) {

			onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;

		}
		else if(cumulativeRotate == false) {

			if(!(getAngleWrapper.getGestureTouchAngle() < 0.5f)) {

				getAngleWrapper.setGestureTouchAngle(-(1f - getAngleWrapper.getGestureTouchAngle()));

			}

			onDownAngleObjectCumulativeBaseOne = getAngleWrapper.getGestureTouchAngle() - onUpAngleGestureBaseOne;
			fullGestureAngleBaseOne = getAngleWrapper.getGestureTouchAngle();

		}//End if(cumulativeRotate == true)

		setReturnType();

		return hgTimeInfo;

	}//End HGTimeDialer.HGTimeInfo doDownDial()


	HGTimeDialer.HGTimeInfo doMoveDial() {

		//Prepare touches for single or dual touch
		if(touchPointerCount > 1) {

			return hgTimeInfo;

		}

		getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));
		setReturnType();

		if(relativeTime == true) {

			if(fullGestureAngleBaseOne < 0f) {

				iSecondRelativeTime.setSecondAngle((1f + fullGestureAngleBaseOne) * (1f / 60f));

			}
			else /* if(fullGestureAngleBaseOne >= 0f) */ {

				iSecondRelativeTime.setSecondAngle(fullGestureAngleBaseOne * (1f / 60f));

			}//End if(fullGestureAngleBaseOne < 0f)

		}//End if(relativeTime == true)

		return hgTimeInfo;

	}//End HGTimeDialer.HGTimeInfo doMoveDial()


	HGTimeDialer.HGTimeInfo doUpDial() {

		//Prepare touches for single or dual touch
		if(touchPointerCount > 1) {

			return hgTimeInfo;

		}

		getAngleWrapper.setGestureTouchAngle(getAngleFromPoint(getAngleWrapper.viewCenterPoint, new Point((int) touchXLocal, (int) touchYLocal)));
		onUpAngleGestureBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle())) % 1;
		setReturnType();

		if(whatHand == GESTURE_SECOND) {

			whatHand = GESTURE_HOUR;

		}

		return hgTimeInfo;

	}//End HGTimeDialer.HGTimeInfo doUpDial()
	/* Bottom of block rotate functions */


	private void setReturnType() {

		setRotationAngleGestureAndReturnDirection();
		hgTimeInfo.setObjectRotationDirection(getAngleWrapper.getObjectRotationDirection());
		hgTimeInfo.setObjectAngleBaseOne(getAngleWrapper.getObjectAngleBaseOne());

		if(secondSnap == 0f) {

			if(fullGestureAngleBaseOne < 0f) {

				this.second = (int) ((1f + fullGestureAngleBaseOne) / (1f / 60f));
				hgTimeInfo.setSecond(this.second);

			}
			else /* if(fullGestureAngleBaseOne >= 0f) */ {

				this.second = (int) (fullGestureAngleBaseOne / (1f / 60f));
				hgTimeInfo.setSecond(this.second);

			}//End if(fullGestureAngleBaseOne < 0f)

		}
		else /* if(secondSnap != 0f) */ {

			this.angleSnapLastBaseOne = this.angleSnapNextBaseOne;
			checkNextSecondSnap();

			if(this.angleSnapLastBaseOne != this.angleSnapNextBaseOne) {

				this.second = (Math.round((1f + angleSnapNextBaseOne) / (1f / 60f)) % 60);
				hgTimeInfo.setSecond(this.second);

			}//End if(this.angleSnapLastBaseOne != this.angleSnapNextBaseOne)

		}//End if(secondSnap == 0f)

		hgTimeInfo.setInternalTime();
		calculateObjectAngleBaseOne();

		if(suppressInvalidate == false) {

			invalidate();

		}

	}//End private void setReturnType()
    /* Bottom of block rotate functions */


	/* Top of block main functions */
	private int setRotationAngleGestureAndReturnDirection() {

		final int[] returnValue = new int[1];
		currentGestureAngleBaseOne = (1 - (onDownAngleObjectCumulativeBaseOne - getAngleWrapper.getGestureTouchAngle()));
		final float angleDifference = (storedGestureAngleBaseOne - currentGestureAngleBaseOne);

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75f)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullGestureAngleBaseOne -= (angleDifference + 1f) % 1f;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullGestureAngleBaseOne += -angleDifference % 1f;

			}

			getAngleWrapper.setObjectRotationDirection(returnValue[0]);

		}
		//Detect count boundary
		else /* if((Math.abs(angleDifference) > 0.75f)) */ {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullGestureAngleBaseOne += 1f - angleDifference;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullGestureAngleBaseOne -= (angleDifference + 1f) % 1f;

			}

			getAngleWrapper.setObjectRotationDirection(returnValue[0]);

		}//End if(!(Math.abs(storedGestureAngleBaseOne - currentGestureAngleBaseOne) > 0.75f))

		if(fullGestureAngleBaseOne < -1f) {

			fullGestureAngleBaseOne = 1f;

		}
		else if(fullGestureAngleBaseOne > 1f) {

			fullGestureAngleBaseOne = -1f;

		}//End if(fullGestureAngleBaseOne < 1f)

		getAngleWrapper.setGestureRotationCount((int) fullGestureAngleBaseOne);
		getAngleWrapper.setGestureAngleBaseOne(fullGestureAngleBaseOne % 1);
		storedObjectCount = (int) (fullGestureAngleBaseOne);
		getAngleWrapper.setObjectRotationCount(storedObjectCount);
		storedGestureAngleBaseOne = currentGestureAngleBaseOne;

		return returnValue[0];

	}//End private int setRotationAngleGestureAndReturnDirection()


	private void calculateObjectAngleBaseOne() {getAngleWrapper.setObjectAngleBaseOne(((getAngleWrapper.getGestureRotationCount() + getAngleWrapper.getGestureAngleBaseOne())) % 1);}


	void checkNextSecondSnap() {

		final float tempAngleSnap = Math.round(getAngleWrapper.getObjectAngleBaseOne() / secondSnap);
		angleSnapNextBaseOne = tempAngleSnap * secondSnap;

		if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity) {

			secondHasSnapped = true;
			hgTimeInfo.setRotateSnapped(true);

		}
		else {

			angleSnapNextBaseOne = getAngleWrapper.getObjectAngleBaseOne();
			secondHasSnapped = false;
			hgTimeInfo.setRotateSnapped(false);

		}//End if(Math.abs(getAngleWrapper.getObjectAngleBaseOne() - angleSnapNextBaseOne) < angleSnapProximity)

	}//End void checkNextSecondSnap()
    /* Bottom of block main functions */


	/* Top of block Accessors */
	public GetAngleWrapper getAngleWrapper() {return this.getAngleWrapper;}
	public boolean getCumulativeRotate() {return this.cumulativeRotate;}
	public int getSecondSnap() {return this.secondSnapField;}
	public float getAngleSnapBaseOne() {return this.secondSnap;}
	public float getAngleSnapProximity() {return this.angleSnapProximity;}
	public float getTouchOffsetX() {return this.touchOffsetX;}
	public float getTouchOffsetY() {return this.touchOffsetY;}
	public boolean hasAngleSnapped() {return secondHasSnapped;}
	public boolean getSuppressInvalidate() {return this.suppressInvalidate;}
	boolean getRelativeTime() {return this.relativeTime;}
    /* Bottom of block Accessors */


	/* Top of block Mutators */
	public void setCumulativeRotate(final boolean cumulativeRotate) {this.cumulativeRotate = cumulativeRotate;}
	public void setSuppressInvalidate(final boolean suppressInvalidate) {HGSecondDialer.suppressInvalidate = suppressInvalidate;}


	public void setSecondSnap(int secondSnap) {

		this.secondSnapField = secondSnap;

		if(secondSnap == 0f) {

			this.secondSnap = 0f;

		}
		else /* if(secondSnap != 0f) */ {

			this.secondSnap = ((1f /60f) * secondSnap) % 1;
			this.angleSnapProximity = (angleSnapProximity) % 1;
			angleSnapProximity = (this.secondSnap /2f) % 1;

		}//End if(secondSnap == 0f)

	}//End public void setSecondSnap(boolean secondSnap)


	public void setHgTimeInfo(HGTimeDialer.HGTimeInfo hgTimeInfo) {this.hgTimeInfo = hgTimeInfo;}
	public void setRelativeTime(boolean relativeTime) {this.relativeTime = relativeTime;}
	void setISecondRelativeTime(ISecondRelativeTime iSecondRelativeTime) {this.iSecondRelativeTime = iSecondRelativeTime;}
    /* Bottom of block Mutators */


	/* Top of block geometry functions */
	public float getAngleFromPoint(final Point centerPoint, final Point touchPoint) {

		float returnVal = 0;

		//+0 - 0.5
		if(touchPoint.x > centerPoint.x) {

			returnVal = (float) (Math.atan2((touchPoint.x - centerPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI);

		}
		//+0.5
		else if(touchPoint.x < centerPoint.x) {

			returnVal = (float)  (1 - (Math.atan2((centerPoint.x - touchPoint.x), (centerPoint.y - touchPoint.y)) * 0.5 / Math.PI));

		}//End if(touchPoint.x > centerPoint.x)

		return returnVal;

	}//End public float getAngleFromPoint(final Point centerPoint, final Point touchPoint)


	public Point getPointFromAngle(final float angle) {return getPointFromAngle(angle, getAngleWrapper.viewCenterPoint.x);}


	public Point getPointFromAngle(final float angle, final float radius) {

		final Point coords = new Point();
		coords.x = (int) (radius - (radius * Math.cos((angle - 0.75f) * 2 * Math.PI)));
		coords.y = (int) (radius - (radius * Math.sin((angle - 0.75f) * 2 * Math.PI)));

		return coords;

	}//End public Point getPointFromAngle(final float angle, final float radius)
    /* Bottom of block geometry functions */

}